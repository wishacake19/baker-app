package com.android.bakerapp.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.models.Order;
import com.android.bakerapp.utilities.DateTimeUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrdersViewHolder> {

    private Context mContext;
    private List<Order> mOrders = new ArrayList<>();
    private OnItemClickListener mItemClickListener;

    private static final String LOG_TAG = OrdersAdapter.class.getSimpleName();

    public OrdersAdapter(Context context, List<Order> orders) {
        this.mContext = context;
        this.mOrders = orders;
    }

    @Override
    public OrdersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_orders_recyclerview_items, parent, false);
        return new OrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrdersViewHolder holder, final int position) {
        Order order = mOrders.get(position);
        try {
            if (order.getImageUrls() != null && order.getImageUrls().get(0) != null) {
                Picasso.get().load(order.getImageUrls().get(0)).fit().placeholder(R.color.colorPlaceholder).into(holder.mImageViewCake);
            } else {
                Picasso.get().load(R.drawable.ic_no_image_available).fit().placeholder(R.color.colorPlaceholder).into(holder.mImageViewCake);
            }
        } catch (Exception e) {
            if (e != null) {
                Log.d(LOG_TAG, e.getMessage());
            }
        }
        String formattedDeliveryDateStr = order.getDeliveryDate();
        long deliveryDate = DateTimeUtils.convertStringDateToLong("dd MMM, yyyy", order.getDeliveryDate());
        if (DateTimeUtils.isToday(deliveryDate)) {
            formattedDeliveryDateStr = "Today";
        } else if (DateTimeUtils.isYesterday(deliveryDate)) {
            formattedDeliveryDateStr = "Yesterday";
        } else if (DateTimeUtils.isTomorrow(deliveryDate)) {
            formattedDeliveryDateStr = "Tomorrow";
        }
        holder.mTextViewDeliveryDateTime.setText(formattedDeliveryDateStr + " • " + order.getDeliveryTime());
        holder.mTextViewPrice.setText("PKR " + String.format("%.0f", order.getTotalAmount()) + "");
        holder.mTextViewDeliveryLocation.setText(order.getDeliveryLocation() + ", " + order.getDeliveryAddress());
        holder.mTextViewUserName.setText(order.getUserName());

        String status = order.getStatus();

        if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_NEW)) {
            holder.mTextViewPrice.setVisibility(View.GONE);
        }
        else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ACCEPTED)) {
            holder.mTextViewPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorOrderStatusAccepted));
        }
        else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_REJECTED)) {
            holder.mTextViewPrice.setText("PKR 0");
            holder.mTextViewPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorOrderStatusRejected));
        }
        else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CONFIRMED)) {
            holder.mTextViewPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorOrderStatusConfirmed));
        }
        else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ONGOING)) {
            holder.mTextViewPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorOrderStatusOngoing));
        }
        else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED)) {
            holder.mTextViewPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorOrderStatusCompleted));
        }

        // Hides the divider from last item
        if (position == getItemCount() - 1) {
            holder.mViewDivider.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public Order getItem(int position) {
        return mOrders.get(position);
    }

    public class OrdersViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageViewCake;
        private TextView mTextViewDeliveryDateTime, mTextViewPrice, mTextViewDeliveryLocation, mTextViewUserName;
        private View mViewDivider;

        public OrdersViewHolder(View view) {
            super(view);
            mImageViewCake = (ImageView) view.findViewById(R.id.img_cake);
            mTextViewDeliveryDateTime = (TextView) view.findViewById(R.id.tv_delivery_date);
            mTextViewPrice = (TextView) view.findViewById(R.id.tv_order_total_amount);
            mTextViewDeliveryLocation = (TextView) view.findViewById(R.id.tv_delivery_location);
            mTextViewUserName = (TextView) view.findViewById(R.id.tv_user_name);
            mViewDivider = (View) view.findViewById(R.id.view_divider);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(v, getAdapterPosition());
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}

