package com.android.bakerapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.ErrorTextWatcher;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEditTextMobileNumber, mEditTextPin;
    private TextView mTextViewHelperMobileNumber, mTextViewHelperPin, mTextViewForgotPin;
    private Button mButtonLogin, mButtonCreateNewAccount;

    private String mMobileNumber, mPin;

    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (SharedPrefSingleton.getInstance(this).isBakerLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
            return;
        }
        if (SharedPrefSingleton.getInstance(this).isBakerAccountCreationInProcess()) {
            Intent intent = new Intent(LoginActivity.this, PhotosActivity.class);
            intent.putExtra(Constants.EXTRA_KEY_IS_PHOTOS_OPENED_AFTER_SPLASH, true);
            startActivity(intent);
            finish();
            return;
        }

        initViews();

        getSupportActionBar().hide();

        mEditTextMobileNumber.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperMobileNumber));
        mEditTextPin.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperPin));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        mEditTextMobileNumber.setText(Constants.MOBILE_NUMBER_PAK_PREFIX);
        Selection.setSelection(mEditTextMobileNumber.getText(), mEditTextMobileNumber.getText().length());

        mEditTextMobileNumber.addTextChangedListener(new TextWatcher() {
            String text;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                text = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().startsWith(Constants.MOBILE_NUMBER_PAK_PREFIX)) {
                    mEditTextMobileNumber.setText(text);
                    Selection.setSelection(mEditTextMobileNumber.getText(), mEditTextMobileNumber.getText().length());
                }
            }
        });

        mButtonLogin.setOnClickListener(this);
        mTextViewForgotPin.setOnClickListener(this);
        mButtonCreateNewAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mButtonLogin) {
            onClickLogin();
        }
        if (view == mTextViewForgotPin) {
            startActivity(new Intent(LoginActivity.this, ForgotPinActivity.class));
        }
        if (view == mButtonCreateNewAccount) {
            startActivity(new Intent(LoginActivity.this, CreateAccountActivity.class));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initViews() {
        mEditTextMobileNumber = (EditText) findViewById(R.id.et_mobile_number);
        mEditTextPin = (EditText) findViewById(R.id.et_pin);
        mTextViewHelperMobileNumber = (TextView) findViewById(R.id.tv_helper_mobile_number);
        mTextViewHelperPin = (TextView) findViewById(R.id.tv_helper_pin);
        mTextViewForgotPin = (TextView) findViewById(R.id.tv_forgot_pin);
        mButtonLogin = (Button) findViewById(R.id.btn_login);
        mButtonCreateNewAccount = (Button) findViewById(R.id.btn_create_new_account);
    }

    private boolean isValid() {
        boolean flag = false;

        mMobileNumber = mEditTextMobileNumber.getText().toString().trim();
        mPin = mEditTextPin.getText().toString().trim();

        if (mMobileNumber.isEmpty()) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is required.", mTextViewHelperMobileNumber);
        } else if (Utils.isMobileNumberValid(mMobileNumber) == false) {
            Utils.showError(mEditTextMobileNumber, "Please enter a valid mobile number.", mTextViewHelperMobileNumber);
        } else if (mPin.isEmpty()) {
            Utils.showError(mEditTextPin, "PIN is required.", mTextViewHelperPin);
        } else if (Utils.isPinValid(mPin) == false) {
            Utils.showError(mEditTextPin, "Please enter a valid PIN.", mTextViewHelperPin);
        } else {
            flag = true;
        }
        return flag;
    }

    private void onClickLogin() {
        if (isValid()) {
            if (!Utils.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                return;
            }
            mProgressDialog.setMessage("Logging in...");
            mProgressDialog.show();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    login();
                    return null;
                }
            }.execute();
        }
    }

    private void login() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        String accountStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_STATUS);

                        // Pending
                        if (accountStatus.equals("0")) {
                            Utils.showOkAlertDialog(LoginActivity.this, "On hold", "Your account status is on hold and is awaiting approval. We'll notify you once your account is activated within the next few hours.", R.drawable.ic_warning);
                            return;
                        }

                        // Rejected
                        if (accountStatus.equals("2")) {
                            Utils.showOkAlertDialog(LoginActivity.this, "Rejected", "Your account request has been rejected because the information you provided isn't correct. For further details please visit your nearest wishacake help center or call us at our helpline (021) 111-111-123.", R.drawable.ic_clear_red);
                            return;
                        }

                        // Blocked
                        if (accountStatus.equals("3")) {
                            Utils.showOkAlertDialog(LoginActivity.this, "Blocked", "Your account has been temporarily blocked. For further details please visit your nearest wishacake help center or call us at our helpline (021) 111-111-123.", R.drawable.ic_block);
                            return;
                        }

                        String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                        String image = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_IMAGE);
                        String firstName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                        String lastName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                        String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                        mMobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                        String cnic = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_CNIC);
                        mPin = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PIN);
                        String locationId = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_ID);
                        String locationName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_NAME);
                        String locationAddress = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                        String locationLatitude = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_LATITUDE);
                        String locationLongitude = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_LONGITUDE);
                        String sliderImage1 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE1);
                        String sliderImage2 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE2);
                        String sliderImage3 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE3);
                        String rating = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_RATING))
                                ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_RATING);
                        String activeStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACTIVE_STATUS);
                        String bakerEarning = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_BAKER_EARNING))
                                ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_BAKER_EARNING);

                        if (activeStatus.equals("1")) {
                            activeStatus = ApiConstants.PARAM_VALUE_ONLINE;
                        } else if (activeStatus.equals("0")) {
                            activeStatus = ApiConstants.PARAM_VALUE_OFFLINE;
                        }

                        SharedPrefSingleton.getInstance(LoginActivity.this).saveBakerData(id, image, firstName, lastName, email,
                                mMobileNumber, cnic, mPin, locationId, locationName, locationAddress, locationLatitude,
                                locationLongitude, sliderImage1, sliderImage2, sliderImage3, rating, bakerEarning, activeStatus);

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("0")) {
                            Utils.showError(mEditTextMobileNumber, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), mTextViewHelperMobileNumber);
                        } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("-1")) {
                            Utils.showError(mEditTextPin, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), mTextViewHelperPin);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(LoginActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_FIREBASE_TOKEN, Utils.getFirebaseToken());
                params.put(ApiConstants.PARAM_MOBILE_NUMBER, mMobileNumber);
                params.put(ApiConstants.PARAM_PIN, mPin);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
