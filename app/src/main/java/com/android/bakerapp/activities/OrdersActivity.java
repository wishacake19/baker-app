package com.android.bakerapp.activities;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.adapters.OrdersAdapter;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.models.Order;
import com.android.bakerapp.utilities.DateTimeUtils;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrdersActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerViewOrderItems;
    private RelativeLayout mRelativeLayoutEmptyView;
    private ProgressBar mProgressBar;
    private ImageView mImageViewEmptyView;
    private TextView mTextViewEmptyView, mTextViewEmptyViewTextButton;

    private List<String> mImageUrls;
    private List<Order> mOrders;
    private OrdersAdapter mOrdersAdapter;

    private String mStatus;

    private static final String LOG_TAG = OrdersActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onNewIntent(getIntent());
        this.initStatus(getIntent());
        setTheme(Utils.getOrdersActivityThemeByStatus(mStatus));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        if (!SharedPrefSingleton.getInstance(this).isBakerLoggedIn()) {
            Toast.makeText(this, "Oops! You are logged out.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(OrdersActivity.this, LoginActivity.class));
            finish();
            return;
        }

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));
        getSupportActionBar().setTitle(mStatus + " orders");

        mTextViewEmptyViewTextButton.setTextColor(ContextCompat.getColor(this, Utils.getOrdersActivityPrimaryColorByStatus(mStatus)));
        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, Utils.getOrdersActivityPrimaryColorByStatus(mStatus)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mProgressBar.setIndeterminateTintList(ColorStateList.valueOf(ContextCompat.getColor(this, Utils.getOrdersActivityPrimaryColorByStatus(mStatus))));
        }

        mImageUrls = new ArrayList<>();
        mOrders = new ArrayList<>();
        mOrdersAdapter = new OrdersAdapter(this, mOrders);
        mOrdersAdapter.setItemClickListener(new OrdersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Order order = mOrdersAdapter.getItem(position);
                Intent orderIntent = new Intent(OrdersActivity.this, OrderDetailsActivity.class);
                orderIntent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) order.getImageUrls());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_ID, order.getId());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, order.getStatus());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_QUANTITY, order.getQuantity());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_POUNDS, order.getPounds());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DESCRIPTION, order.getDescription());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_CONTACT_NUMBER, order.getContactNumber());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_LOCATION, order.getDeliveryLocation());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_ADDRESS, order.getDeliveryAddress());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_DATE, order.getDeliveryDate());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_TIME, order.getDeliveryTime());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_SUBTOTAL, order.getSubTotal());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_CHARGES, order.getDeliveryCharges());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_TOTAL_AMOUNT, order.getTotalAmount());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_BAKER_EARNING, order.getBakerEarning());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_COMPANY_EARNING, order.getCompanyEarning());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_PAYMENT_METHOD, order.getPaymentMethod());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_REJECTED_REASON, order.getRejectedReason());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_ORDER_RATING, order.getOrderRating());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_USER_ID, order.getUserId());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_USER_NAME, order.getUserName());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_USER_EMAIL, order.getUserEmail());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER, false);
                startActivity(orderIntent);
            }
        });

        mRecyclerViewOrderItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerViewOrderItems.setAdapter(mOrdersAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeAsyncTaskOrders();
            }
        });

        mTextViewEmptyViewTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mTextViewEmptyViewTextButton.getText().toString().equals(getString(R.string.shared_try_again))) {
                    executeAsyncTaskOrders();
                }
            }
        });

        executeAsyncTaskOrders();
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerViewOrderItems = (RecyclerView) findViewById(R.id.rv_orders);
        mRelativeLayoutEmptyView = (RelativeLayout) findViewById(R.id.relative_layout_empty_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mImageViewEmptyView = (ImageView) findViewById(R.id.img_empty_view);
        mTextViewEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        mTextViewEmptyViewTextButton = (TextView) findViewById(R.id.tv_empty_view_text_button);
    }

    private void initStatus(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.EXTRA_KEY_ORDER_STATUS)) {
                mStatus = extras.getString(Constants.EXTRA_KEY_ORDER_STATUS);
            }
        }
    }

    private void stopRefreshingSwipeLayout() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void toggleRecyclerView(int recyclerViewVisibility) {
        mRecyclerViewOrderItems.setVisibility(recyclerViewVisibility);
    }

    private void showEmptyView(int imageVisibility, int textButtonVisibility, int progressBarVisibility, String textViewText, String textButtonText) {
        mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        mImageViewEmptyView.setVisibility(imageVisibility);
        mTextViewEmptyViewTextButton.setVisibility(textButtonVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mTextViewEmptyView.setText(textViewText);
        mTextViewEmptyViewTextButton.setText(textButtonText);
    }

    private void hideEmptyView() {
        mRelativeLayoutEmptyView.setVisibility(View.GONE);
    }

    public void executeAsyncTaskOrders() {
        mOrders.clear();
        toggleRecyclerView(View.GONE);
        showEmptyView(View.GONE, View.GONE, View.VISIBLE, "", "");

        if (!Utils.isNetworkAvailable(this)) {
            stopRefreshingSwipeLayout();
            showEmptyView(View.GONE, View.VISIBLE, View.GONE, getString(R.string.no_internet_connection), getString(R.string.shared_try_again));
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                getOrdersFromServer();
                return null;
            }
        }.execute();
    }

    private void getOrdersFromServer() {
        final List<Order> orders = new ArrayList<>();
        final String loggedInBakerId = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_ORDERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideEmptyView();
                mProgressBar.setVisibility(View.GONE);
                stopRefreshingSwipeLayout();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String responseStatus = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (responseStatus.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        // Get the json array as a response
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);
                        if (responseArray.length() > 0) {
                            // Iterate through the array
                            for (int i = 0; i < responseArray.length(); i++) {
                                List<String> orderImageUrls = new ArrayList<>();
                                // Get json object inside the array index wise
                                JSONObject orderJsonObject = responseArray.getJSONObject(i);
                                // Get order details of order json object index wise
                                String id = orderJsonObject.getString(ApiConstants.PARAM_ID);
                                String image1 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE1);
                                String image2 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE2);
                                String image3 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE3);
                                String image4 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE4);
                                String image5 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE5);
                                String image6 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE6);
                                String image7 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE7);
                                String image8 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE8);
                                String image9 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE9);
                                String image10 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE10);
                                long date = DateTimeUtils.convertStringDateToLong("yyyy-MM-dd HH:mm:ss", orderJsonObject.getString(ApiConstants.PARAM_CREATED_AT));
                                String status = Utils.getOrderStatusValueByOrderStatusCode(orderJsonObject.getString(ApiConstants.PARAM_STATUS));
                                String quantity = orderJsonObject.getString(ApiConstants.PARAM_QUANTITY);
                                String pounds = orderJsonObject.getString(ApiConstants.PARAM_POUNDS);
                                String description = orderJsonObject.getString(ApiConstants.PARAM_DESCRIPTION);
                                String contactNumber = orderJsonObject.getString(ApiConstants.PARAM_CONTACT_NUMBER);
                                String deliveryLocation = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_LOCATION);
                                String deliveryAddress = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_ADDRESS);
                                String deliveryDate = DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd", "dd MMM, yyyy", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_DATE));
                                String deliveryTime = DateTimeUtils.getFormattedDateTimeString("HH:mm:ss", "hh:mm a", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_TIME));
                                double subtotal = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL));
                                double deliveryCharges = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES));
                                double totalAmount = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT));
                                double bakerEarning = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_BAKER_EARNING))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_BAKER_EARNING));
                                double companyEarning = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_COMPANY_EARNING))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_COMPANY_EARNING));
                                String paymentMethod = (orderJsonObject.getString(ApiConstants.PARAM_PAYMENT_METHOD).equals("1"))
                                        ? ApiConstants.PARAM_VALUE_CASH : ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD;
                                String rejectedReason = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON))
                                        ? "You haven't provided the reason for rejecting this order." : orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON);
                                double orderRating = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING));
                                String userId = orderJsonObject.getString(ApiConstants.PARAM_USER_ID);
                                String fn = orderJsonObject.getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = orderJsonObject.getString(ApiConstants.PARAM_LAST_NAME);
                                String email = orderJsonObject.getString(ApiConstants.PARAM_EMAIL);

                                if (!Utils.isStringEmptyOrNull(image1)) {
                                    orderImageUrls.add(image1);
                                }
                                if (!Utils.isStringEmptyOrNull(image2)) {
                                    orderImageUrls.add(image2);
                                }
                                if (!Utils.isStringEmptyOrNull(image3)) {
                                    orderImageUrls.add(image3);
                                }
                                if (!Utils.isStringEmptyOrNull(image4)) {
                                    orderImageUrls.add(image4);
                                }
                                if (!Utils.isStringEmptyOrNull(image5)) {
                                    orderImageUrls.add(image5);
                                }
                                if (!Utils.isStringEmptyOrNull(image6)) {
                                    orderImageUrls.add(image6);
                                }
                                if (!Utils.isStringEmptyOrNull(image7)) {
                                    orderImageUrls.add(image7);
                                }
                                if (!Utils.isStringEmptyOrNull(image8)) {
                                    orderImageUrls.add(image8);
                                }
                                if (!Utils.isStringEmptyOrNull(image9)) {
                                    orderImageUrls.add(image9);
                                }
                                if (!Utils.isStringEmptyOrNull(image10)) {
                                    orderImageUrls.add(image10);
                                }

                                orders.add(new Order(id, orderImageUrls, date, status, quantity, pounds, description, contactNumber,
                                        deliveryLocation, deliveryAddress, deliveryDate, deliveryTime, subtotal, deliveryCharges,
                                        totalAmount, bakerEarning, companyEarning, paymentMethod, rejectedReason, orderRating, userId,
                                        (fn + " " + ln), email));
                            }
                            hideEmptyView();
                            toggleRecyclerView(View.VISIBLE);
                            mOrders.addAll(orders);
                            mRecyclerViewOrderItems.setAdapter(mOrdersAdapter);
                            mOrdersAdapter.notifyDataSetChanged();
                        } else {
                            toggleRecyclerView(View.GONE);
                            showEmptyView(View.VISIBLE, View.GONE, View.GONE, "No " + mStatus.toLowerCase() + " orders found.", "");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                stopRefreshingSwipeLayout();
                toggleRecyclerView(View.GONE);
                showEmptyView(View.GONE, View.VISIBLE, View.GONE, "Sorry, something went wrong. Please try again.", getString(R.string.shared_try_again));
                if (error != null) {
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_BAKER_ID, loggedInBakerId);
                params.put(ApiConstants.PARAM_STATUS, mStatus);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
