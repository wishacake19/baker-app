package com.android.bakerapp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.PermissionUtils;
import com.android.bakerapp.utilities.UriRealPathUtils;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PhotosActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mTextViewHeading;
    private ImageView mImageViewPhoto;
    private FrameLayout mFrameLayoutTakeReTakePhoto, mFrameLayoutNext, mFrameLayoutSkip;
    private Button mButtonTakeReTakePhoto, mButtonNext, mButtonSkip;

    private ProgressDialog mProgressDialog;

    private String mFirstName, mLastName, mEmail, mMobileNumber, mCnic, mPin, mLocationId, mLocationName, mImage,
            mLocationAddress, mLocationLatitude, mLocationLongitude, mCnicFrontBase64, mCnicBackBase64, mProfilePhotoBase64;

    private static String sAppName;

    // For SDK < 24
    private Uri mPhotoUri;

    private static final int REQUEST_CODE_CAMERA = 02;
    private static final String LOG_TAG = PhotosActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        getSupportActionBar().hide();

        initViews();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        sAppName = getString(R.string.app_name);

        loadBakerInfoFromSharedPref();

        mTextViewHeading.setText(Html.fromHtml("Upload a photo of your<br><strong>CNIC Front Side</strong>"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.EXTRA_KEY_IS_PHOTOS_OPENED_AFTER_SPLASH)) {
                if (extras.getBoolean(Constants.EXTRA_KEY_IS_PHOTOS_OPENED_AFTER_SPLASH)) {
                    Toast.makeText(this, "Welcome back " + mFirstName + " " + mLastName + "!\nYou can continue signing up from here.", Toast.LENGTH_SHORT).show();
                }
            }
        }

        mButtonTakeReTakePhoto.setOnClickListener(this);
        mButtonNext.setOnClickListener(this);
        mButtonSkip.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (mTextViewHeading.getText().toString().contains("profile")) {
            mTextViewHeading.setText(Html.fromHtml("Upload a photo of your<br><strong>CNIC Back Side</strong>"));
            mFrameLayoutSkip.setVisibility(View.GONE);
            mFrameLayoutNext.setVisibility(View.GONE);
            mButtonNext.setText("Next");
            mButtonTakeReTakePhoto.setText("Take photo");
            mImageViewPhoto.setImageDrawable(ContextCompat.getDrawable(PhotosActivity.this, R.drawable.ic_placeholder_bg));
            mProfilePhotoBase64 = "";
        } else if (mTextViewHeading.getText().toString().contains("Back")) {
            mTextViewHeading.setText(Html.fromHtml("Upload a photo of your<br><strong>CNIC Front Side</strong>"));
            mFrameLayoutNext.setVisibility(View.GONE);
            mButtonTakeReTakePhoto.setText("Take photo");
            mImageViewPhoto.setImageDrawable(ContextCompat.getDrawable(PhotosActivity.this, R.drawable.ic_placeholder_bg));
            mCnicBackBase64 = "";
        } else if (mTextViewHeading.getText().toString().contains("Front")) {
            // Closes the whole app
            ActivityCompat.finishAffinity(PhotosActivity.this); // Clear the previous activities stack
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mButtonTakeReTakePhoto) {
            if (PermissionUtils.checkCameraPermission(PhotosActivity.this)) {
                openCamera();
            }
        }
        if (view == mButtonNext) {
            if (mTextViewHeading.getText().toString().contains("Front")) {
                mTextViewHeading.setText(Html.fromHtml("Upload a photo of your<br><strong>CNIC Back Side</strong>"));
                mFrameLayoutNext.setVisibility(View.GONE);
                mButtonTakeReTakePhoto.setText("Take photo");
                mImageViewPhoto.setImageDrawable(ContextCompat.getDrawable(PhotosActivity.this, R.drawable.ic_placeholder_bg));
            } else if (mTextViewHeading.getText().toString().contains("Back")) {
                mTextViewHeading.setText(Html.fromHtml("Upload your <strong>profile photo</strong>"));
                mFrameLayoutNext.setVisibility(View.GONE);
                mButtonTakeReTakePhoto.setText("Take photo");
                mFrameLayoutSkip.setVisibility(View.VISIBLE);
                mImageViewPhoto.setImageDrawable(ContextCompat.getDrawable(PhotosActivity.this, R.drawable.ic_placeholder_bg));
            } else if (mTextViewHeading.getText().toString().contains("profile")) {
                // Hit server at the last step
                onClickFinishOrSkip();
            }
        }
        if (view == mButtonSkip) {
            // Hit server with profile photo left empty
            mProfilePhotoBase64 = "";
            onClickFinishOrSkip();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_CAMERA_OR_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (cameraPermission) {
                    if (!PermissionUtils.checkGalleryPermission(PhotosActivity.this)) {
                        boolean writeStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        if (writeStoragePermission) {
                            openCamera();
                        }
                    }
                    openCamera();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
                        mImageViewPhoto.setImageBitmap(imageBitmap);
//                        Uri uri = Uri.parse(Utils.getPathFromBitmap(PhotosActivity.this, imageBitmap));
//                        Picasso.get().load(uri).fit().placeholder(R.color.colorDivider).into(mImageViewPhoto);

                        if (mTextViewHeading.getText().toString().contains("Front")) {
                            mCnicFrontBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                        } else if (mTextViewHeading.getText().toString().contains("Back")) {
                            mCnicBackBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                        } else if (mTextViewHeading.getText().toString().contains("profile")) {
                            mProfilePhotoBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                        }
                    } else {
                        Toast.makeText(this, "An unknown error has occurred while saving the image. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.v(LOG_TAG + "Image Camera || ", "Data is null");
                }
            } else {
//                Picasso.get().load(mPhotoUri).fit().placeholder(R.color.colorDivider).into(mImageViewPhoto);
                mImageViewPhoto.setImageURI(mPhotoUri);

                String imagePath = UriRealPathUtils.getUriRealPath(this, mPhotoUri);
                Bitmap imageBitmap = null;
                if (!imagePath.isEmpty()) {
                    imageBitmap = BitmapFactory.decodeFile(imagePath);
                }
                if (mTextViewHeading.getText().toString().contains("Front")) {
                    mCnicFrontBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                } else if (mTextViewHeading.getText().toString().contains("Back")) {
                    mCnicBackBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                } else if (mTextViewHeading.getText().toString().contains("profile")) {
                    mProfilePhotoBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                }
            }
            mFrameLayoutSkip.setVisibility(View.GONE);
            mFrameLayoutNext.setVisibility(View.VISIBLE);
            mButtonTakeReTakePhoto.setText("Re take");
            if (mTextViewHeading.getText().toString().contains("profile")) {
                mButtonNext.setText("Finish");
            }
        }
    }

    private void initViews() {
        mTextViewHeading = (TextView) findViewById(R.id.tv_heading);
        mImageViewPhoto = (ImageView) findViewById(R.id.img_photo);
        mFrameLayoutTakeReTakePhoto = (FrameLayout) findViewById(R.id.frame_layout_take_re_take_photo);
        mFrameLayoutNext = (FrameLayout) findViewById(R.id.frame_layout_next);
        mFrameLayoutSkip = (FrameLayout) findViewById(R.id.frame_layout_skip);
        mButtonTakeReTakePhoto = (Button) findViewById(R.id.btn_take_re_take_photo);
        mButtonNext = (Button) findViewById(R.id.btn_next);
        mButtonSkip = (Button) findViewById(R.id.btn_skip);
    }

    private void loadBakerInfoFromSharedPref() {
        mImage = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_IMAGE);
        mFirstName = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_FIRST_NAME);
        mLastName = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LAST_NAME);
        mEmail = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_EMAIL);
        mMobileNumber = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_MOBILE_NUMBER);
        mCnic = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_CNIC);
        mPin = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_PIN);
        mLocationId = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ID);
        mLocationName = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_NAME);
        mLocationAddress = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ADDRESS);
        mLocationLatitude = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LATITUDE);
        mLocationLongitude = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LONGITUDE);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFileForSDK24AndAbove();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(LOG_TAG, "IOException || " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(PhotosActivity.this,
                        getPackageName() + ".fileprovider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
            }
        } else {
            mPhotoUri = Uri.fromFile(createImageFileForSDKBelow24());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
            startActivityForResult(intent, REQUEST_CODE_CAMERA);
        }
    }

    private File createImageFileForSDK24AndAbove() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + timeStamp;
        File storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(fileName, ".jpg", storageDirectory);
    }

    private File createImageFileForSDKBelow24() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), sAppName);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    private void onClickFinishOrSkip() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            return;
        }
        mProgressDialog.setMessage("Creating account...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                signUp();
                return null;
            }
        }.execute();
    }

    private void signUp() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_SIGN_UP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(PhotosActivity.this);
                        builder.setTitle("Thanks, " + mFirstName + " " + mLastName + "!");
                        builder.setMessage("We have received your information and is being reviewed. We'll notify you once your account is activated within the next few hours.");
                        builder.setCancelable(true);

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                SharedPrefSingleton.getInstance(PhotosActivity.this).clearBakerData();
                                ActivityCompat.finishAffinity(PhotosActivity.this); // Clear the previous activities stack
                                finish();
                                startActivity(new Intent(PhotosActivity.this, LoginActivity.class));
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        Toast.makeText(PhotosActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(PhotosActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_FIREBASE_TOKEN, Utils.getFirebaseToken());
                if (Utils.isStringEmptyOrNull(mProfilePhotoBase64)) {
                    params.put(ApiConstants.PARAM_IMAGE, "null");
                } else {
                    params.put(ApiConstants.PARAM_IMAGE, mProfilePhotoBase64);
                }
                params.put(ApiConstants.PARAM_FIRST_NAME, mFirstName);
                params.put(ApiConstants.PARAM_LAST_NAME, mLastName);
                params.put(ApiConstants.PARAM_EMAIL, mEmail.toLowerCase());
                params.put(ApiConstants.PARAM_MOBILE_NUMBER, mMobileNumber);
                params.put(ApiConstants.PARAM_CNIC, Utils.getFormattedCNIC(mCnic));
                params.put(ApiConstants.PARAM_PIN, mPin);
                params.put(ApiConstants.PARAM_CNIC_FRONT, mCnicFrontBase64);
                params.put(ApiConstants.PARAM_CNIC_BACK, mCnicBackBase64);
                params.put(ApiConstants.PARAM_LOCATION_ID, mLocationId);
                params.put(ApiConstants.PARAM_LOCATION_NAME, mLocationName);
                params.put(ApiConstants.PARAM_LOCATION_ADDRESS, mLocationAddress);
                params.put(ApiConstants.PARAM_LOCATION_LATITUDE, mLocationLatitude);
                params.put(ApiConstants.PARAM_LOCATION_LONGITUDE, mLocationLongitude);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
