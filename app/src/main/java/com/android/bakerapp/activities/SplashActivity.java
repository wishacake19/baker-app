package com.android.bakerapp.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.bakerapp.R;

public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener {

    //    private final int SPLASH_SCREEN_DISPLAY_LENGTH = 3500;
    private ImageView mImageViewLogo;
    private Animation mAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);

        // hides the action bar
        getSupportActionBar().hide();

//        new Handler().postDelayed(new Runnable(){
//            @Override
//            public void run() {
//                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
//                startActivity(i);
//                finish();
//            }
//        }, SPLASH_SCREEN_DISPLAY_LENGTH);

        mImageViewLogo = (ImageView) findViewById(R.id.img_logo);

        mAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        mAnimation.setAnimationListener(this);
        mImageViewLogo.startAnimation(mAnimation);
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }
}
