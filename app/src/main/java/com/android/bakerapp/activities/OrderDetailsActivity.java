package com.android.bakerapp.activities;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.adapters.CakeImageAdapter;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.ExpandableTextView;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.DateTimeUtils;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderDetailsActivity extends AppCompatActivity
        implements View.OnClickListener, View.OnLongClickListener {

    private ScrollView mScrollView;
    private RecyclerView mRecyclerViewOrderImagesItems;
    private TextView mTextViewEmptyView, mTextViewEmptyViewTextButton,
            mTextViewDeliveryDateTime, mTextViewStatus, mTextViewId, mTextViewQuantity, mTextViewPounds,
            mTextViewName, mTextViewEmailAddress, mTextViewContactNumber,
            mTextViewDeliveryLocation, mTextViewDeliveryAddress, mTextViewDeliveryDate, mTextViewDeliveryTime,
            mTextViewSubtotal, mTextViewDeliveryCharges, mTextViewTotalAmount, mTextViewBakerEarning,
            mTextViewCompanyEarning, mTextViewOrderRating;
    private ExpandableTextView mExpandableTextViewDescription;
    private ImageView mImageViewArrowContact, mImageViewArrowDelivery, mImageViewArrowPayment, mImageViewPaymentMethod;
    private RelativeLayout mRelativeLayoutRootEmptyView, mRelativeLayoutEmptyView, mRelativeLayoutContactInfo,
            mRelativeLayoutDeliveryInfo, mRelativeLayoutPaymentInfo;
    private LinearLayout mLinearLayoutContactInfo, mLinearLayoutDeliveryInfo, mLinearLayoutPaymentInfoRootContainer,
            mLinearLayoutPaymentInfo, mLinearLayoutEarnings, mLinearLayoutButtonsAcceptReject, mLinearLayoutButtonReAccept,
            mLinearLayoutButtonViewReason, mLinearLayoutButtonMarkAsOngoing,
            mLinearLayoutButtonMarkAsCompleted, mLinearLayoutOrderRating;
    private ProgressBar mProgressBar;
    private Button mButtonAccept, mButtonReject, mButtonReAccept, mButtonViewReason, mButtonMarkAsOngoing, mButtonMarkAsCompleted;

    private ProgressDialog mProgressDialog;
    private CakeImageAdapter mCakeImageAdapter;

    private List<String> mOrderImageUrls;
    private String mQuantity, mPounds, mStatus = "", mId, mDescription, mContactNumber, mDeliveryLocation,
            mDeliveryAddress, mDeliveryDate, mDeliveryTime, mPaymentMethod, mRejectedReason, mUserId, mUserName, mUserEmail;
    private String mSubtotalStr, mDeliveryChargesStr, mTotalAmountStr, mBakerEarningStr, mCompanyEarningStr;
    private double mSubtotal, mDeliveryCharges, mTotalAmount, mBakerEarning, mCompanyEarning, mOrderRating;
    private boolean mIsContactInfoCollapsed = true, mIsDeliveryInfoCollapsed = true, mIsPaymentInfoCollapsed = true,
            mIsOrderDataToBeLoadedFromServer = false;

    private static final String LOG_TAG = OrderDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        if (!SharedPrefSingleton.getInstance(this).isBakerLoggedIn()) {
            Toast.makeText(this, "Oops! You are logged out.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(OrderDetailsActivity.this, LoginActivity.class));
            finish();
            return;
        }

        initViews();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(OrderDetailsActivity.this, R.drawable.ic_arrow_back_white));

        mOrderImageUrls = new ArrayList<>();
        onNewIntent(getIntent());
        prepareOrderData(getIntent());
        mCakeImageAdapter = new CakeImageAdapter(this, mOrderImageUrls);
        mRecyclerViewOrderImagesItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewOrderImagesItems.setAdapter(mCakeImageAdapter);

        mTextViewEmptyViewTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTextViewEmptyViewTextButton.getText().toString().equalsIgnoreCase(getString(R.string.shared_try_again))) {
                    mTextViewEmptyViewTextButton.setVisibility(View.GONE);
                    getOrderDataFromServer();
                }
            }
        });

        mRelativeLayoutContactInfo.setOnClickListener(this);
        mRelativeLayoutPaymentInfo.setOnClickListener(this);
        mRelativeLayoutDeliveryInfo.setOnClickListener(this);
        mTextViewContactNumber.setOnClickListener(this);
        mTextViewEmailAddress.setOnClickListener(this);
        mTextViewDeliveryLocation.setOnClickListener(this);
        mTextViewContactNumber.setOnLongClickListener(this);
        mTextViewEmailAddress.setOnLongClickListener(this);
        mTextViewDeliveryLocation.setOnLongClickListener(this);
        mButtonAccept.setOnClickListener(this);
        mButtonReject.setOnClickListener(this);
        mButtonReAccept.setOnClickListener(this);
        mButtonViewReason.setOnClickListener(this);
        mButtonMarkAsOngoing.setOnClickListener(this);
        mButtonMarkAsCompleted.setOnClickListener(this);
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        onClickBack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onClickBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onLongClick(View view) {
        ClipData clipData = null;
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

        if (view == mTextViewDeliveryLocation) {
            clipData = ClipData.newPlainText("location", mDeliveryLocation);
            Toast.makeText(this, "Shipping location copied to clipboard.", Toast.LENGTH_SHORT).show();
        }
        if (view == mTextViewEmailAddress) {
            clipData = ClipData.newPlainText("email_address", mUserEmail);
            Toast.makeText(this, "Email address copied to clipboard.", Toast.LENGTH_SHORT).show();
        }
        if (view == mTextViewContactNumber) {
            clipData = ClipData.newPlainText("contact_number", mContactNumber);
            Toast.makeText(this, "Contact number copied to clipboard.", Toast.LENGTH_SHORT).show();
        }
        clipboardManager.setPrimaryClip(clipData);
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view == mRelativeLayoutContactInfo) {
            if (mIsContactInfoCollapsed) {
                // Expand
                mIsContactInfoCollapsed = false;
                mImageViewArrowContact.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_up));
                Utils.expand(mLinearLayoutContactInfo, 3);
            } else {
                // Collapse
                mIsContactInfoCollapsed = true;
                mImageViewArrowContact.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
                Utils.collapse(mLinearLayoutContactInfo, 3);
            }
        }
        if (view == mRelativeLayoutPaymentInfo) {
            if (mIsPaymentInfoCollapsed) {
                // Expand
                mIsPaymentInfoCollapsed = false;
                mImageViewArrowPayment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_up));
                Utils.expand(mLinearLayoutPaymentInfo, 3);
            } else {
                // Collapse
                mIsPaymentInfoCollapsed = true;
                mImageViewArrowPayment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
                Utils.collapse(mLinearLayoutPaymentInfo, 3);
            }
        }
        if (view == mRelativeLayoutDeliveryInfo) {
            if (mIsDeliveryInfoCollapsed) {
                // Expand
                mIsDeliveryInfoCollapsed = false;
                mImageViewArrowDelivery.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_up));
                Utils.expand(mLinearLayoutDeliveryInfo, 3);
            } else {
                // Collapse
                mIsDeliveryInfoCollapsed = true;
                mImageViewArrowDelivery.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
                Utils.collapse(mLinearLayoutDeliveryInfo, 3);
            }
        }
        if (view == mTextViewDeliveryLocation) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("geo:0,0?q= " + mDeliveryLocation));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mTextViewEmailAddress) {
            String[] emailAddresses = new String[1];
            emailAddresses[0] = mUserEmail;

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, emailAddresses);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mTextViewContactNumber) {
            final CharSequence[] items = {"Call", "SMS"};
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("Call")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + mContactNumber));
                        if (intent.resolveActivity(getPackageManager()) != null) {
                            startActivity(intent);
                        }
                    } else if (items[item].equals("SMS")) {
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse("smsto:")); // This ensures only SMS apps respond
                        intent.putExtra("address", mContactNumber);
                        if (intent.resolveActivity(getPackageManager()) != null) {
                            startActivity(intent);
                        }
                    }
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.setTitle(mContactNumber);
            alertDialog.setCancelable(true);
            alertDialog.show();
        }
        if (view == mButtonAccept) {
            Intent intent = new Intent(OrderDetailsActivity.this, AcceptRejectActivity.class);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_ACTIVITY_MODE, Constants.ACTIVITY_MODE_ACCEPT);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_DATE_AND_TIME, mTextViewDeliveryDateTime.getText().toString());
            intent.putExtra(Constants.EXTRA_KEY_ORDER_ID, mId);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_USER_ID, mUserId);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_BAKER_ID, SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
            startActivity(intent);
        }
        if (view == mButtonReject) {
            Intent intent = new Intent(OrderDetailsActivity.this, AcceptRejectActivity.class);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_ACTIVITY_MODE, Constants.ACTIVITY_MODE_REJECT);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_DATE_AND_TIME, mTextViewDeliveryDateTime.getText().toString());
            intent.putExtra(Constants.EXTRA_KEY_ORDER_ID, mId);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_USER_ID, mUserId);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_BAKER_ID, SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
            startActivity(intent);
        }
        if (view == mButtonReAccept) {
            Intent intent = new Intent(OrderDetailsActivity.this, AcceptRejectActivity.class);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_ACTIVITY_MODE, Constants.ACTIVITY_MODE_RE_ACCEPT);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_DATE_AND_TIME, mTextViewDeliveryDateTime.getText().toString());
            intent.putExtra(Constants.EXTRA_KEY_ORDER_ID, mId);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_USER_ID, mUserId);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_BAKER_ID, SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
            intent.putExtra(Constants.EXTRA_KEY_ORDER_SUBTOTAL, mSubtotal);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_CHARGES, mDeliveryCharges);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_TOTAL_AMOUNT, mTotalAmount);
            startActivity(intent);
        }
        if (view == mButtonViewReason) {
            Utils.showOkAlertDialog(OrderDetailsActivity.this, "Rejected reason", mRejectedReason, 0);
        }
        if (view == mButtonMarkAsOngoing) {
            mSubtotalStr = String.format("%.0f", mSubtotal);
            mDeliveryChargesStr = String.format("%.0f", mDeliveryCharges);
            mTotalAmountStr = String.format("%.0f", mTotalAmount);
            showConfirmAlertDialog("Ongoing confirmation", "Are you sure want to mark this order as ongoing?", ApiConstants.PARAM_VALUE_ONGOING);
        }
        if (view == mButtonMarkAsCompleted) {
            mSubtotalStr = String.format("%.0f", mSubtotal);
            mDeliveryChargesStr = String.format("%.0f", mDeliveryCharges);
            mTotalAmountStr = String.format("%.0f", mTotalAmount);
            showConfirmAlertDialog("Complete confirmation", "Are you sure want to mark this order as completed?", ApiConstants.PARAM_VALUE_COMPLETED);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        prepareOrderData(intent);
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        // Empty view
        mRelativeLayoutRootEmptyView = (RelativeLayout) findViewById(R.id.relative_layout_root_empty_view);
        mRelativeLayoutEmptyView = (RelativeLayout) findViewById(R.id.relative_layout_empty_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mTextViewEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        mTextViewEmptyViewTextButton = (TextView) findViewById(R.id.tv_empty_view_text_button);

        // Cake info views
        mRecyclerViewOrderImagesItems = (RecyclerView) findViewById(R.id.rv_order_images_items);
        mTextViewDeliveryDateTime = (TextView) findViewById(R.id.tv_order_delivery_date_time);
        mTextViewStatus = (TextView) findViewById(R.id.tv_order_status);
        mTextViewId = (TextView) findViewById(R.id.tv_order_id);
        mTextViewQuantity = (TextView) findViewById(R.id.tv_quantity);
        mTextViewPounds = (TextView) findViewById(R.id.tv_pounds);
        mExpandableTextViewDescription = (ExpandableTextView) findViewById(R.id.etv_order_description);

        // Contact info views
        mRelativeLayoutContactInfo = (RelativeLayout) findViewById(R.id.relative_layout_contact_info);
        mLinearLayoutContactInfo = (LinearLayout) findViewById(R.id.linear_layout_contact_info);
        mImageViewArrowContact = (ImageView) findViewById(R.id.img_arrow_contact);
        mTextViewName = (TextView) findViewById(R.id.tv_name);
        mTextViewEmailAddress = (TextView) findViewById(R.id.tv_email_address);
        mTextViewContactNumber = (TextView) findViewById(R.id.tv_contact_number);

        // Payment info views
        mLinearLayoutPaymentInfoRootContainer = (LinearLayout) findViewById(R.id.linear_layout_payment_info_root_container);
        mRelativeLayoutPaymentInfo = (RelativeLayout) findViewById(R.id.relative_layout_payment_info);
        mImageViewArrowPayment = (ImageView) findViewById(R.id.img_arrow_payment);
        mLinearLayoutPaymentInfo = (LinearLayout) findViewById(R.id.linear_layout_payment_info);
        mTextViewSubtotal = (TextView) findViewById(R.id.tv_order_subtotal);
        mTextViewDeliveryCharges = (TextView) findViewById(R.id.tv_order_delivery_charges);
        mTextViewTotalAmount = (TextView) findViewById(R.id.tv_order_total_amount);
        mLinearLayoutEarnings = (LinearLayout) findViewById(R.id.linear_layout_earnings);
        mTextViewBakerEarning = (TextView) findViewById(R.id.tv_order_baker_earning);
        mTextViewCompanyEarning = (TextView) findViewById(R.id.tv_order_company_earning);
        mImageViewPaymentMethod = (ImageView) findViewById(R.id.img_payment_method);

        // Delivery info views
        mRelativeLayoutDeliveryInfo = (RelativeLayout) findViewById(R.id.relative_layout_delivery_info);
        mImageViewArrowDelivery = (ImageView) findViewById(R.id.img_arrow_delivery);
        mLinearLayoutDeliveryInfo = (LinearLayout) findViewById(R.id.linear_layout_delivery_info);
        mTextViewDeliveryLocation = (TextView) findViewById(R.id.tv_order_delivery_location);
        mTextViewDeliveryAddress = (TextView) findViewById(R.id.tv_order_delivery_address);
        mTextViewDeliveryDate = (TextView) findViewById(R.id.tv_order_delivery_date);
        mTextViewDeliveryTime = (TextView) findViewById(R.id.tv_order_delivery_time);

        // Other views
        mLinearLayoutButtonsAcceptReject = (LinearLayout) findViewById(R.id.linear_layout_buttons_accept_reject);
        mLinearLayoutButtonReAccept = (LinearLayout) findViewById(R.id.linear_layout_button_change_price);
        mLinearLayoutButtonViewReason = (LinearLayout) findViewById(R.id.linear_layout_button_view_reason);
        mLinearLayoutButtonMarkAsOngoing = (LinearLayout) findViewById(R.id.linear_layout_button_mark_as_ongoing);
        mLinearLayoutButtonMarkAsCompleted = (LinearLayout) findViewById(R.id.linear_layout_button_mark_as_completed);
        mLinearLayoutOrderRating = (LinearLayout) findViewById(R.id.linear_layout_order_rating);
        mTextViewOrderRating = (TextView) findViewById(R.id.tv_order_rating_text);
        mButtonAccept = (Button) findViewById(R.id.btn_accept);
        mButtonReject = (Button) findViewById(R.id.btn_reject);
        mButtonReAccept = (Button) findViewById(R.id.btn_re_accept);
        mButtonViewReason = (Button) findViewById(R.id.btn_view_reason);
        mButtonMarkAsOngoing = (Button) findViewById(R.id.btn_mark_as_ongoing);
        mButtonMarkAsCompleted = (Button) findViewById(R.id.btn_mark_as_completed);
        mScrollView = (ScrollView) findViewById(R.id.sv_scrollview);
    }

    private void onClickBack() {
        Log.d(LOG_TAG, "mIsOrderDataToBeLoadedFromServer: " + mIsOrderDataToBeLoadedFromServer);
        if (mIsOrderDataToBeLoadedFromServer == true) {
            ActivityCompat.finishAffinity(OrderDetailsActivity.this);
            startActivity(new Intent(OrderDetailsActivity.this, MainActivity.class));
        } else {
            finish();
        }
    }

    private void showConfirmAlertDialog(String title, String msg, final String statusParamValue) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                updateOrderStatus(statusParamValue);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setOrderData() {
        long deliveryDate = DateTimeUtils.convertStringDateToLong("dd MMM, yyyy", mDeliveryDate);
        String formattedDeliveryDateStr = mDeliveryDate;
        if (DateTimeUtils.isToday(deliveryDate)) {
            formattedDeliveryDateStr = "Today";
        } else if (DateTimeUtils.isYesterday(deliveryDate)) {
            formattedDeliveryDateStr = "Yesterday";
        } else if (DateTimeUtils.isTomorrow(deliveryDate)) {
            formattedDeliveryDateStr = "Tomorrow";
        }
        mTextViewDeliveryDateTime.setText(formattedDeliveryDateStr + " • " + mDeliveryTime);

        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_NEW)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_NEW,
                    R.color.colorOrderStatusPending, R.drawable.order_status_pending_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_REJECTED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_REJECTED,
                    R.color.colorOrderStatusRejected, R.drawable.order_status_rejected_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ACCEPTED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_ACCEPTED,
                    R.color.colorOrderStatusAccepted, R.drawable.order_status_accepted_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CANCELLED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_CANCELLED,
                    R.color.colorOrderStatusCancelled, R.drawable.order_status_cancelled_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CONFIRMED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_CONFIRMED,
                    R.color.colorOrderStatusConfirmed, R.drawable.order_status_confirmed_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ONGOING)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_ONGOING,
                    R.color.colorOrderStatusOngoing, R.drawable.order_status_ongoing_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_COMPLETED,
                    R.color.colorOrderStatusCompleted, R.drawable.order_status_completed_drawable);
        }
        mTextViewId.setText("ORDER # " + mId);
        mTextViewQuantity.setText(Html.fromHtml("<font color='#757575'>Quantity</font>"
                + "<br><font color='#212121'>" + mQuantity + "</font>"));
        mTextViewPounds.setText(Html.fromHtml("<font color='#757575'>Pounds</font>"
                + "<br><font color='#212121'>" + mPounds + "</font>"));
        if (!Utils.isStringEmptyOrNull(mDescription)) {
            String transformedDescription = mDescription.replaceAll("\n", "<br>");
            mExpandableTextViewDescription.setText(Html.fromHtml("<font color='#757575'>Description</font>"
                    + "<br><font color='#212121'>" + transformedDescription + "</font>"));
        } else {
            mExpandableTextViewDescription.setVisibility(View.GONE);
            mExpandableTextViewDescription.setText("");
        }

        // contact info
        mTextViewName.setText(mUserName);
        mTextViewEmailAddress.setText(mUserEmail);
        mTextViewContactNumber.setText(mContactNumber);

        // shipping info
        mTextViewDeliveryLocation.setText(mDeliveryLocation);
        mTextViewDeliveryAddress.setText(mDeliveryAddress);
        mTextViewDeliveryDate.setText(formattedDeliveryDateStr);
        mTextViewDeliveryTime.setText(mDeliveryTime);

        mTextViewSubtotal.setText(String.format("%.0f", mSubtotal) + "");
        mTextViewDeliveryCharges.setText(String.format("%.0f", mDeliveryCharges) + "");
        mTextViewTotalAmount.setText("PKR " + String.format("%.0f", mTotalAmount) + "");

        if (mPaymentMethod.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD)) {
            mImageViewPaymentMethod.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_card));
        } else if (mPaymentMethod.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CASH)) {
            mImageViewPaymentMethod.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cash));
        }

        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_NEW)) {
            mLinearLayoutButtonsAcceptReject.setVisibility(View.VISIBLE);
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ACCEPTED)) {
            mLinearLayoutButtonReAccept.setVisibility(View.VISIBLE);
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_REJECTED)) {
            mTextViewTotalAmount.setTextColor(ContextCompat.getColor(this, R.color.colorOrderStatusRejected));
            mLinearLayoutButtonViewReason.setVisibility(View.VISIBLE);
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CONFIRMED)) {
            mLinearLayoutButtonMarkAsOngoing.setVisibility(View.VISIBLE);
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CANCELLED)) {
            adjustMarginBottomScrollView(0);
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ONGOING)) {
            mLinearLayoutButtonMarkAsCompleted.setVisibility(View.VISIBLE);
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED)) {
            adjustMarginBottomScrollView(0);
            mTextViewOrderRating.setText(Html.fromHtml("<font color='#2196F3'>You've received <strong>"
                    + new DecimalFormat("0.0").format(Double.parseDouble(String.valueOf(mOrderRating))) +
                    "</strong> rating on this order.</font>"));
            mLinearLayoutOrderRating.setVisibility(View.VISIBLE);
            mLinearLayoutEarnings.setVisibility(View.VISIBLE);
            mTextViewBakerEarning.setText("PKR " + String.format("%.0f", mBakerEarning) + "");
            mTextViewCompanyEarning.setText("PKR " + String.format("%.0f", mCompanyEarning) + "");
        }
    }

    private void adjustMarginBottomScrollView(int bottomMargin) {
        // Margin bottom for scrollview as there no bottom button
        final float scale = getResources().getDisplayMetrics().density;
        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) mScrollView.getLayoutParams();
        p.setMargins(0, 0, 0, bottomMargin);
        mScrollView.setLayoutParams(p);
    }

    private void prepareOrderData(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mId = extras.getString(Constants.EXTRA_KEY_ORDER_ID);
            mIsOrderDataToBeLoadedFromServer = extras.getBoolean(Constants.EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER);

            if (mIsOrderDataToBeLoadedFromServer == false) {
                mIsOrderDataToBeLoadedFromServer = false;
                mOrderImageUrls = extras.getStringArrayList(Constants.EXTRA_KEY_ORDER_IMAGES);
                mStatus = extras.getString(Constants.EXTRA_KEY_ORDER_STATUS);
                mQuantity = extras.getString(Constants.EXTRA_KEY_ORDER_QUANTITY);
                mPounds = extras.getString(Constants.EXTRA_KEY_ORDER_POUNDS);
                mDescription = extras.getString(Constants.EXTRA_KEY_ORDER_DESCRIPTION);
                mContactNumber = extras.getString(Constants.EXTRA_KEY_ORDER_CONTACT_NUMBER);
                mDeliveryLocation = extras.getString(Constants.EXTRA_KEY_ORDER_DELIVERY_LOCATION);
                mDeliveryAddress = extras.getString(Constants.EXTRA_KEY_ORDER_DELIVERY_ADDRESS);
                mDeliveryDate = extras.getString(Constants.EXTRA_KEY_ORDER_DELIVERY_DATE);
                mDeliveryTime = extras.getString(Constants.EXTRA_KEY_ORDER_DELIVERY_TIME);
                mPaymentMethod = extras.getString(Constants.EXTRA_KEY_ORDER_PAYMENT_METHOD);
                mRejectedReason = extras.getString(Constants.EXTRA_KEY_ORDER_REJECTED_REASON);
                mOrderRating = extras.getDouble(Constants.EXTRA_KEY_ORDER_ORDER_RATING);
                mSubtotal = extras.getDouble(Constants.EXTRA_KEY_ORDER_SUBTOTAL);
                mDeliveryCharges = extras.getDouble(Constants.EXTRA_KEY_ORDER_DELIVERY_CHARGES);
                mTotalAmount = extras.getDouble(Constants.EXTRA_KEY_ORDER_TOTAL_AMOUNT);
                mBakerEarning = extras.getDouble(Constants.EXTRA_KEY_ORDER_BAKER_EARNING);
                mCompanyEarning = extras.getDouble(Constants.EXTRA_KEY_ORDER_COMPANY_EARNING);
                mUserId = extras.getString(Constants.EXTRA_KEY_ORDER_USER_ID);
                mUserName = extras.getString(Constants.EXTRA_KEY_ORDER_USER_NAME);
                mUserEmail = extras.getString(Constants.EXTRA_KEY_ORDER_USER_EMAIL);
                setOrderData();
            } else {
                mIsOrderDataToBeLoadedFromServer = true;
                getOrderDataFromServer();
            }
        }
    }

    private void showEmptyView(int textButtonVisibility, int progressBarVisibility, String textViewText) {
        mRelativeLayoutRootEmptyView.setVisibility(View.VISIBLE);
        mTextViewEmptyViewTextButton.setVisibility(textButtonVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mTextViewEmptyView.setText(textViewText);
    }

    private void toggleMainDataViews(int visibility) {
        mScrollView.setVisibility(visibility);
    }

    private void hideEmptyView() {
        mRelativeLayoutRootEmptyView.setVisibility(View.GONE);
    }

    private void getOrderDataFromServer() {
        toggleMainDataViews(View.GONE);
        showEmptyView(View.GONE, View.VISIBLE, "");

        if (!Utils.isNetworkAvailable(this)) {
            showEmptyView(View.VISIBLE, View.GONE, getString(R.string.no_internet_connection));
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_SINGLE_ORDER, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideEmptyView();
                        mProgressBar.setVisibility(View.GONE);

                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String responseStatus = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (responseStatus.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                // Get the json array as a response
                                JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);
                                JSONObject orderJsonObject = responseArray.getJSONObject(0);

                                // Get order details of order json object
                                String image1 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE1);
                                String image2 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE2);
                                String image3 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE3);
                                String image4 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE4);
                                String image5 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE5);
                                String image6 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE6);
                                String image7 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE7);
                                String image8 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE8);
                                String image9 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE9);
                                String image10 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE10);
                                mStatus = Utils.getOrderStatusValueByOrderStatusCode(orderJsonObject.getString(ApiConstants.PARAM_STATUS));
                                mQuantity = orderJsonObject.getString(ApiConstants.PARAM_QUANTITY);
                                mPounds = orderJsonObject.getString(ApiConstants.PARAM_POUNDS);
                                mDescription = orderJsonObject.getString(ApiConstants.PARAM_DESCRIPTION);
                                mContactNumber = orderJsonObject.getString(ApiConstants.PARAM_CONTACT_NUMBER);
                                mUserId = orderJsonObject.getString(ApiConstants.PARAM_USER_ID);
                                mUserName = orderJsonObject.getString(ApiConstants.PARAM_FIRST_NAME) + " " + orderJsonObject.getString(ApiConstants.PARAM_LAST_NAME);
                                mUserEmail = orderJsonObject.getString(ApiConstants.PARAM_EMAIL);
                                mDeliveryLocation = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_LOCATION);
                                mDeliveryAddress = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_ADDRESS);
                                mDeliveryDate = DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd", "dd MMM, yyyy", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_DATE));
                                mDeliveryTime = DateTimeUtils.getFormattedDateTimeString("HH:mm:ss", "hh:mm a", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_TIME));
                                mSubtotal = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL));
                                mDeliveryCharges = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES));
                                mTotalAmount = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT));
                                mBakerEarning = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_BAKER_EARNING))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_BAKER_EARNING));
                                mCompanyEarning = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_COMPANY_EARNING))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_COMPANY_EARNING));
                                mPaymentMethod = (orderJsonObject.getString(ApiConstants.PARAM_PAYMENT_METHOD).equals("1"))
                                        ? ApiConstants.PARAM_VALUE_CASH : ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD;
                                mRejectedReason = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON))
                                        ? "You haven't provided the reason for rejecting this order." : orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON);
                                mOrderRating = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING));

                                if (!Utils.isStringEmptyOrNull(image1)) {
                                    mOrderImageUrls.add(image1);
                                }
                                if (!Utils.isStringEmptyOrNull(image2)) {
                                    mOrderImageUrls.add(image2);
                                }
                                if (!Utils.isStringEmptyOrNull(image3)) {
                                    mOrderImageUrls.add(image3);
                                }
                                if (!Utils.isStringEmptyOrNull(image4)) {
                                    mOrderImageUrls.add(image4);
                                }
                                if (!Utils.isStringEmptyOrNull(image5)) {
                                    mOrderImageUrls.add(image5);
                                }
                                if (!Utils.isStringEmptyOrNull(image6)) {
                                    mOrderImageUrls.add(image6);
                                }
                                if (!Utils.isStringEmptyOrNull(image7)) {
                                    mOrderImageUrls.add(image7);
                                }
                                if (!Utils.isStringEmptyOrNull(image8)) {
                                    mOrderImageUrls.add(image8);
                                }
                                if (!Utils.isStringEmptyOrNull(image9)) {
                                    mOrderImageUrls.add(image9);
                                }
                                if (!Utils.isStringEmptyOrNull(image10)) {
                                    mOrderImageUrls.add(image10);
                                }

                                hideEmptyView();
                                toggleMainDataViews(View.VISIBLE);
                                setOrderData();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        toggleMainDataViews(View.GONE);
                        showEmptyView(View.VISIBLE, View.GONE, "Sorry, something went wrong. Please try again.");
                        if (error != null) {
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        HashMap<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ORDER_ID, mId);
                        params.put(ApiConstants.PARAM_ORDER_FOR, ApiConstants.PARAM_VALUE_BAKER);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(OrderDetailsActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    private void updateOrderStatus(final String statusAction) {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.setMessage("Processing...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE_ORDER_STATUS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);
                            String responseMsg = rootJsonObject.getString(ApiConstants.KEY_RESPONSE);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                String toastMsg = "[" + mTextViewDeliveryDateTime.getText().toString() + "] ";
                                if (statusAction.equals(ApiConstants.PARAM_VALUE_ONGOING)) {
                                    toastMsg += "Order marked as ongoing successfully.";
                                } else if (statusAction.equals(ApiConstants.PARAM_VALUE_COMPLETED)) {
                                    toastMsg += "Order marked as completed successfully.";
                                }
                                Toast.makeText(OrderDetailsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                                ActivityCompat.finishAffinity(OrderDetailsActivity.this);
                                startActivity(new Intent(OrderDetailsActivity.this, MainActivity.class));
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(OrderDetailsActivity.this, responseMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(OrderDetailsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ORDER_ID, mId);
                        params.put(ApiConstants.PARAM_USER_ID, mUserId);
                        params.put(ApiConstants.PARAM_BAKER_ID, SharedPrefSingleton.getInstance(OrderDetailsActivity.this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                        params.put(ApiConstants.PARAM_SUBTOTAL, mSubtotalStr);
                        params.put(ApiConstants.PARAM_DELIVERY_CHARGES, mDeliveryChargesStr);
                        params.put(ApiConstants.PARAM_TOTAL_AMOUNT, mTotalAmountStr);
                        params.put(ApiConstants.PARAM_REJECTED_REASON, "null");
                        params.put(ApiConstants.PARAM_STATUS, statusAction);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(OrderDetailsActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }
}
