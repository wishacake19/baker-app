package com.android.bakerapp.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;

    private ScrollView mScrollView;
    private CircleImageView mCircleImageViewProfilePhoto;
    private LinearLayout mLinearLayoutProfile, mLinearLayoutChangePin, mLinearLayoutEarnings, mLinearLayoutSettings, mLinearLayoutHelp,
            mLinearLayoutMyOrdersSubMenu, mLinearLayoutNew, mLinearLayoutAccepted, mLinearLayoutRejected, mLinearLayoutConfirmed,
            mLinearLayoutCancelled, mLinearLayoutOngoing, mLinearLayoutCompleted;
    private RelativeLayout mRelativeLayoutMyOrders;
    private ImageView mImageViewArrowMyOrders, mImageViewActiveStatus, mImageViewRefresh;
    private TextView mTextViewName, mTextViewRating, mTextViewCountNew, mTextViewCountAccepted, mTextViewCountRejected, mTextViewCountConfirmed,
            mTextViewCountCancelled, mTextViewCountOngoing, mTextViewCountCompleted, mTextViewEarning, mTextViewActiveStatus, mTextViewWelcome;
    private Switch mSwitchActiveStatus;

    private String mBakerEarning, mActiveStatus;
    private boolean mIsMyOrdersCollapsed = true;

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!SharedPrefSingleton.getInstance(this).isBakerLoggedIn()) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
            return;
        }

        initViews();

        setSupportActionBar(mToolbar);

        setUpNavigationView();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
            return;
        }
        super.onBackPressed();

        // Closes the whole app
        ActivityCompat.finishAffinity(MainActivity.this); // Clear the previous activities stack
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUpHeaderView();
        toggleActiveStatus(false);
    }

    @Override
    public void onClick(View view) {
        if (view == mRelativeLayoutMyOrders) {
            if (mIsMyOrdersCollapsed) {
                // Expand
                mIsMyOrdersCollapsed = false;
                mImageViewArrowMyOrders.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_up));
                Utils.expand(mLinearLayoutMyOrdersSubMenu, 2);
            } else {
                // Collapse
                mIsMyOrdersCollapsed = true;
                mImageViewArrowMyOrders.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
                Utils.collapse(mLinearLayoutMyOrdersSubMenu, 2);
            }
        }
        if (view == mLinearLayoutNew || view == mLinearLayoutAccepted || view == mLinearLayoutRejected || view == mLinearLayoutConfirmed || view == mLinearLayoutCancelled || view == mLinearLayoutOngoing || view == mLinearLayoutCompleted) {
            Intent intent = new Intent(MainActivity.this, OrdersActivity.class);
            if (view == mLinearLayoutNew) {
                intent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, ApiConstants.PARAM_VALUE_NEW);
            } else if (view == mLinearLayoutAccepted) {
                intent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, ApiConstants.PARAM_VALUE_ACCEPTED);
            } else if (view == mLinearLayoutRejected) {
                intent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, ApiConstants.PARAM_VALUE_REJECTED);
            } else if (view == mLinearLayoutConfirmed) {
                intent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, ApiConstants.PARAM_VALUE_CONFIRMED);
            } else if (view == mLinearLayoutCancelled) {
                intent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, ApiConstants.PARAM_VALUE_CANCELLED);
            } else if (view == mLinearLayoutOngoing) {
                intent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, ApiConstants.PARAM_VALUE_ONGOING);
            } else if (view == mLinearLayoutCompleted) {
                intent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, ApiConstants.PARAM_VALUE_COMPLETED);
            }
            resetSideNavToDefaultState();
            startActivity(intent);
        }
        if (view == mLinearLayoutProfile) {
            resetSideNavToDefaultState();
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        }
        if (view == mLinearLayoutChangePin) {
            resetSideNavToDefaultState();
            startActivity(new Intent(MainActivity.this, ChangePinActivity.class));
        }
//        if (view == mLinearLayoutEarnings) {
//            resetSideNavToDefaultState();
//            Toast.makeText(this, "Not implemented yet.", Toast.LENGTH_SHORT).show();
//        }
        if (view == mLinearLayoutSettings) {
            resetSideNavToDefaultState();
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        }
        if (view == mLinearLayoutHelp) {
            resetSideNavToDefaultState();
            Intent intent = new Intent(MainActivity.this, HelpActivity.class);
            intent.putExtra(Constants.KEY_IS_HELP_OPENED_FOR_REPORTING, false);
            startActivity(intent);

//            Uri uri = Uri.parse("http://feedpakistan.com/wishacake/help.html");
//            Intent intentHelp = new Intent(Intent.ACTION_VIEW, uri);
//            if (intentHelp.resolveActivity(getPackageManager()) != null) {
//                startActivity(intentHelp);
//            }
        }
        if (view == mImageViewRefresh) {
            toggleActiveStatus(true);
        }
    }

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mScrollView = (ScrollView) findViewById(R.id.sv_scrollView);
        mCircleImageViewProfilePhoto = (CircleImageView) findViewById(R.id.civ_profile_photo);
        mTextViewName = (TextView) findViewById(R.id.tv_name);
        mTextViewRating = (TextView) findViewById(R.id.tv_rating);
        mTextViewWelcome = (TextView) findViewById(R.id.tv_welcome_text);
        mRelativeLayoutMyOrders = (RelativeLayout) findViewById(R.id.relative_layout_my_orders);
        mImageViewArrowMyOrders = (ImageView) findViewById(R.id.img_arrow_my_orders);
        mLinearLayoutMyOrdersSubMenu = (LinearLayout) findViewById(R.id.linear_layout_my_orders_sub_menu);
        mLinearLayoutNew = (LinearLayout) findViewById(R.id.linear_layout_new);
        mLinearLayoutAccepted = (LinearLayout) findViewById(R.id.linear_layout_accepted);
        mLinearLayoutRejected = (LinearLayout) findViewById(R.id.linear_layout_rejected);
        mLinearLayoutConfirmed = (LinearLayout) findViewById(R.id.linear_layout_confirmed);
        mLinearLayoutCancelled = (LinearLayout) findViewById(R.id.linear_layout_cancelled);
        mLinearLayoutOngoing = (LinearLayout) findViewById(R.id.linear_layout_ongoing);
        mLinearLayoutCompleted = (LinearLayout) findViewById(R.id.linear_layout_completed);
        mTextViewCountNew = (TextView) findViewById(R.id.tv_count_new);
        mTextViewCountAccepted = (TextView) findViewById(R.id.tv_count_accepted);
        mTextViewCountRejected = (TextView) findViewById(R.id.tv_count_rejected);
        mTextViewCountConfirmed = (TextView) findViewById(R.id.tv_count_confirmed);
        mTextViewCountCancelled = (TextView) findViewById(R.id.tv_count_cancelled);
        mTextViewCountOngoing = (TextView) findViewById(R.id.tv_count_ongoing);
        mTextViewCountCompleted = (TextView) findViewById(R.id.tv_count_completed);
        mLinearLayoutProfile = (LinearLayout) findViewById(R.id.linear_layout_profile);
        mLinearLayoutChangePin = (LinearLayout) findViewById(R.id.linear_layout_change_pin);
        mLinearLayoutEarnings = (LinearLayout) findViewById(R.id.linear_layout_earnings);
        mTextViewEarning = (TextView) findViewById(R.id.tv_baker_earning);
        mLinearLayoutSettings = (LinearLayout) findViewById(R.id.linear_layout_settings);
        mLinearLayoutHelp = (LinearLayout) findViewById(R.id.linear_layout_help);
        mImageViewActiveStatus = (ImageView) findViewById(R.id.img_active_status);
        mTextViewActiveStatus = (TextView) findViewById(R.id.tv_active_status);
        mSwitchActiveStatus = (Switch) findViewById(R.id.switch_active_status);
        mImageViewRefresh = (ImageView) findViewById(R.id.img_refresh);
    }

    private void resetSideNavToDefaultState() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
        }
        mIsMyOrdersCollapsed = true;
        mImageViewArrowMyOrders.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        Utils.collapse(mLinearLayoutMyOrdersSubMenu, 2);
        mScrollView.scrollTo(0, 0);
    }

    private void setUpHeaderView() {
        final String image = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_IMAGE);
        String firstName = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_FIRST_NAME);
        String lastName = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LAST_NAME);
        String name;
        if (firstName == null || lastName == null) {
            name = "Name";
        } else {
            name = firstName + " " + lastName;
        }
        mTextViewWelcome.setText("Welcome\n" + name + "!");
        String rating = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_RATING);
        if (rating == null || rating.equals("0")) {
            rating = "N/A";
        } else {
            rating = new DecimalFormat("0.0").format(Double.parseDouble(rating));
        }

        if (!Utils.isStringEmptyOrNull(image)) {
            Picasso.get().load(image).fit().placeholder(R.drawable.ic_default_profile_pic).into(mCircleImageViewProfilePhoto);
        } else {
            Picasso.get().load(R.drawable.ic_default_profile_pic).placeholder(R.drawable.ic_default_profile_pic).into(mCircleImageViewProfilePhoto);
        }

        mTextViewName.setText(name);
        mTextViewRating.setText(rating);

        getOrdersCount();

        getEarning();
//        if (Double.parseDouble(mBakerEarning) > 1000) {
//            mBakerEarning = "1000+";
//        }
        mTextViewEarning.setText("PKR " + mBakerEarning);
    }

    private void setUpNavigationView() {
        mRelativeLayoutMyOrders.setOnClickListener(this);
        mLinearLayoutProfile.setOnClickListener(this);
        mLinearLayoutChangePin.setOnClickListener(this);
//        mLinearLayoutEarnings.setOnClickListener(this);
        mLinearLayoutSettings.setOnClickListener(this);
        mLinearLayoutHelp.setOnClickListener(this);
        mLinearLayoutNew.setOnClickListener(this);
        mLinearLayoutAccepted.setOnClickListener(this);
        mLinearLayoutRejected.setOnClickListener(this);
        mLinearLayoutConfirmed.setOnClickListener(this);
        mLinearLayoutCancelled.setOnClickListener(this);
        mLinearLayoutOngoing.setOnClickListener(this);
        mLinearLayoutCompleted.setOnClickListener(this);
        mImageViewRefresh.setOnClickListener(this);

        mSwitchActiveStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (compoundButton.isPressed()) {
                    if (!Utils.isNetworkAvailable(getApplicationContext())) {
                        updateUIActiveStatus(mActiveStatus.equals(ApiConstants.PARAM_VALUE_ONLINE) ? true : false);
                        Toast.makeText(MainActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mActiveStatus = isChecked ? ApiConstants.PARAM_VALUE_ONLINE : ApiConstants.PARAM_VALUE_OFFLINE;
                    updateActiveStatus(isChecked, true);
                }
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout, mToolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                onStart();
//                Utils.changeStatusBarBackground(MainActivity.this, ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                onStart();
//                Utils.changeStatusBarBackground(MainActivity.this, Color.TRANSPARENT);
            }
        };

        // Setting the actionBarToggle to drawer layout
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);

        // Calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void getEarning() {
        mBakerEarning = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_EARNING);
        if (mBakerEarning == null) {
            mBakerEarning = "0";
        }
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_BAKER_EARNING, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
//                                mBakerEarning = rootJsonObject.getString(ApiConstants.KEY_RESPONSE);
                                mBakerEarning = Utils.isStringEmptyOrNull(rootJsonObject.getString(ApiConstants.KEY_RESPONSE)) ? "0" : rootJsonObject.getString(ApiConstants.KEY_RESPONSE);
                                SharedPrefSingleton.getInstance(MainActivity.this).saveBakerEarning(mBakerEarning);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_BAKER_ID, SharedPrefSingleton.getInstance(MainActivity.this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(MainActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    private void getOrdersCount() {
        mTextViewCountNew.setText("");
        mTextViewCountAccepted.setText("");
        mTextViewCountRejected.setText("");
        mTextViewCountConfirmed.setText("");
        mTextViewCountCancelled.setText("");
        mTextViewCountOngoing.setText("");
        mTextViewCountCompleted.setText("");

        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_BAKER_ORDERS_COUNT, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (status.equals(ApiConstants.STATUS_SUCCESS)) {
                                JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                                String newOrders = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_NEW))
                                        ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_NEW);
                                String acceptedOrders = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_ACCEPTED))
                                        ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_ACCEPTED);
                                String rejectedOrders = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_REJECTED))
                                        ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_REJECTED);
                                String confirmedOrders = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_CONFIRMED))
                                        ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_CONFIRMED);
                                String cancelledOrders = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_CANCELLED))
                                        ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_CANCELLED);
                                String ongoingOrders = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_ONGOING))
                                        ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_ONGOING);
                                String completedOrders = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_COMPLETED))
                                        ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_VALUE_COMPLETED);

                                if (Integer.parseInt(newOrders) > 99) {
                                    newOrders = "99+";
                                }
                                if (Integer.parseInt(acceptedOrders) > 99) {
                                    acceptedOrders = "99+";
                                }
                                if (Integer.parseInt(rejectedOrders) > 99) {
                                    rejectedOrders = "99+";
                                }
                                if (Integer.parseInt(confirmedOrders) > 99) {
                                    confirmedOrders = "99+";
                                }
                                if (Integer.parseInt(cancelledOrders) > 99) {
                                    cancelledOrders = "99+";
                                }
                                if (Integer.parseInt(ongoingOrders) > 99) {
                                    ongoingOrders = "99+";
                                }
                                if (Integer.parseInt(completedOrders) > 99) {
                                    completedOrders = "99+";
                                }

                                mTextViewCountNew.setText(newOrders);
                                mTextViewCountAccepted.setText(acceptedOrders);
                                mTextViewCountRejected.setText(rejectedOrders);
                                mTextViewCountConfirmed.setText(confirmedOrders);
                                mTextViewCountCancelled.setText(cancelledOrders);
                                mTextViewCountOngoing.setText(ongoingOrders);
                                mTextViewCountCompleted.setText(completedOrders);
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
//                                Toast.makeText(MainActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_BAKER_ID, SharedPrefSingleton.getInstance(MainActivity.this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(MainActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    private void updateActiveStatus(final boolean isChecked, final boolean showErrorMsg) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE_ACTIVE_STATUS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                updateUIActiveStatus(isChecked);
                                SharedPrefSingleton.getInstance(MainActivity.this).saveBakerActiveStatus(mActiveStatus);
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                updateUIActiveStatus(isChecked);
                                Toast.makeText(MainActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            updateUIActiveStatus(isChecked);
                            if (showErrorMsg) {
                                String toastMsg = "Sorry, something went wrong. Please try again.";
                                Toast.makeText(MainActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            }
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ID, SharedPrefSingleton.getInstance(MainActivity.this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                        params.put(ApiConstants.PARAM_ACTIVE_STATUS, mActiveStatus);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(MainActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    private void toggleActiveStatus(boolean showErrorMsg) {
        mActiveStatus = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ACTIVE_STATUS);
        if (mActiveStatus == null) {
            mActiveStatus = ApiConstants.PARAM_VALUE_OFFLINE;
        }
        boolean isChecked = mActiveStatus.equals(ApiConstants.PARAM_VALUE_ONLINE) ? true : false;
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            updateUIActiveStatus(isChecked);
            if (showErrorMsg) {
                Toast.makeText(MainActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }
            return;
        }
        updateActiveStatus(isChecked, showErrorMsg);
    }

    private void updateUIActiveStatus(boolean isChecked) {
        mSwitchActiveStatus.setChecked(isChecked);
        if (isChecked) {
            mTextViewActiveStatus.setText("Online");
            mImageViewActiveStatus.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_active_status_online));
            mActiveStatus = ApiConstants.PARAM_VALUE_ONLINE;
        } else {
            mTextViewActiveStatus.setText("Offline");
            mImageViewActiveStatus.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_active_status_offline));
            mActiveStatus = ApiConstants.PARAM_VALUE_OFFLINE;
        }
    }
}
