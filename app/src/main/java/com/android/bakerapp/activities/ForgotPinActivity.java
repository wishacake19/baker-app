package com.android.bakerapp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.ErrorTextWatcher;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPinActivity extends AppCompatActivity {

    private EditText mEditTextEmail;
    private TextView mTextViewHelperEmail;
    private Button mButtonSend;

    private String mEmail;

    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = ForgotPinActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pin);

        getSupportActionBar().hide();

        initViews();

        mEditTextEmail.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperEmail));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSend();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initViews() {
        mEditTextEmail = (EditText) findViewById(R.id.et_email);
        mTextViewHelperEmail = (TextView) findViewById(R.id.tv_helper_email);
        mButtonSend = (Button) findViewById(R.id.btn_send);
    }

    private boolean isValid() {
        boolean flag = false;
        mEmail = mEditTextEmail.getText().toString().trim();

        if (mEmail.isEmpty()) {
            Utils.showError(mEditTextEmail, "Email address is required.", mTextViewHelperEmail);
        } else if (Utils.isEmailValid(mEmail) == false) {
            Utils.showError(mEditTextEmail, "Please enter a valid email address.", mTextViewHelperEmail);
        } else {
            flag = true;
        }
        return flag;
    }

    private void onClickSend() {
        if (isValid()) {
            if (!Utils.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(ForgotPinActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
                return;
            }
            mProgressDialog.setMessage("Requesting PIN reset email...");
            mProgressDialog.show();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_FORGOT_PIN, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            mProgressDialog.dismiss();
                            try {
                                JSONObject rootJsonObject = new JSONObject(response);
                                String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                                if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPinActivity.this);
                                    builder.setTitle("");
                                    builder.setMessage("We have sent an email to \"" + mEmail + "\" containing instructions on how to reset your PIN.");
                                    builder.setCancelable(true);

                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });

                                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            finish();
                                        }
                                    });

                                    AlertDialog alert = builder.create();
                                    alert.show();
                                } else {
                                    String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                                    if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("0")) {
                                        Utils.showError(mEditTextEmail, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                                mTextViewHelperEmail);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mProgressDialog.dismiss();
                            if (error != null) {
                                String toastMsg = "Sorry, something went wrong. Please try again.";
                                Toast.makeText(ForgotPinActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                                Log.e(LOG_TAG, error.toString());
                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put(ApiConstants.PARAM_EMAIL, mEmail.toLowerCase());
                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleyRequestHandlerSingleton.getInstance(ForgotPinActivity.this).addToRequestQueue(stringRequest);
                    return null;
                }
            }.execute();
        }
    }
}
