package com.android.bakerapp.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.Constants;
import com.facebook.drawee.backends.pipeline.Fresco;

import me.relex.photodraweeview.PhotoDraweeView;

public class FullscreenImageActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView mTextViewEmptyView;
    private PhotoDraweeView mPhotoDraweeViewImage;

    private String mImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(FullscreenImageActivity.this);
        setContentView(R.layout.activity_fullscreen_image);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(FullscreenImageActivity.this, R.drawable.ic_arrow_back_white));
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        if (!Utils.isNetworkAvailable(getApplicationContext())) {
//            getSupportActionBar().setTitle("");
//            mTextViewEmptyView.setVisibility(View.VISIBLE);
//            mTextViewEmptyView.setText(getString(R.string.no_internet_connection));
//        } else {
            mTextViewEmptyView.setVisibility(View.GONE);
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                if (extras.containsKey(Constants.EXTRA_KEY_FULLSCREEN_IMAGE)) {
                    getSupportActionBar().setTitle("");
                    mImage = extras.getString(Constants.EXTRA_KEY_FULLSCREEN_IMAGE);
                    mPhotoDraweeViewImage.setPhotoUri(Uri.parse(mImage));
                }
            }
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTextViewEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        mPhotoDraweeViewImage = (PhotoDraweeView) findViewById(R.id.pdv_img);
    }
}
