package com.android.bakerapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.BuildConfig;
import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;

    private LinearLayout mLinearLayoutRateUs, mLinearLayoutVisitWebsite, mLinearLayoutTermsOfService, mLinearLayoutPrivacyPolicy, mLinearLayoutLogout;
    private TextView mTextViewEmail, mTextViewAppVersion;
    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = SettingsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initViews();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        mLinearLayoutRateUs.setOnClickListener(this);
        mLinearLayoutVisitWebsite.setOnClickListener(this);
        mLinearLayoutTermsOfService.setOnClickListener(this);
        mLinearLayoutPrivacyPolicy.setOnClickListener(this);
        mLinearLayoutLogout.setOnClickListener(this);

        mTextViewEmail.setText(SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_EMAIL));
        mTextViewAppVersion.setText(BuildConfig.VERSION_NAME);
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mLinearLayoutRateUs) {
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mLinearLayoutVisitWebsite) {
            Uri uri = Uri.parse("http://feedpakistan.com/wishacake/");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mLinearLayoutTermsOfService) {
            Uri uri = Uri.parse("http://feedpakistan.com/wishacake/terms-of-service.html");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mLinearLayoutPrivacyPolicy) {
            Uri uri = Uri.parse("http://feedpakistan.com/wishacake/privacy-policy.html");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mLinearLayoutLogout) {
            onClickLogout();
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mLinearLayoutRateUs = (LinearLayout) findViewById(R.id.linear_layout_rate_us);
        mLinearLayoutVisitWebsite = (LinearLayout) findViewById(R.id.linear_layout_visit_website);
        mLinearLayoutTermsOfService = (LinearLayout) findViewById(R.id.linear_layout_terms_of_service);
        mLinearLayoutPrivacyPolicy = (LinearLayout) findViewById(R.id.linear_layout_privacy_policy);
        mLinearLayoutLogout = (LinearLayout) findViewById(R.id.linear_layout_logout);
        mTextViewEmail = (TextView) findViewById(R.id.tv_email);
        mTextViewAppVersion = (TextView) findViewById(R.id.tv_app_version);
    }

    private void onClickLogout() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("");
//        builder.setMessage("Are you sure you want to log out?");
//        builder.setCancelable(true);
//
//        builder.setPositiveButton("Log out", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                if (!Utils.isNetworkAvailable(getApplicationContext())) {
//                    Toast.makeText(SettingsActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
//                } else {
//                    dialog.dismiss();
//                    makeActiveStatusOffline();
//                }
//            }
//        });
//
//        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//
//        AlertDialog alert = builder.create();
//        alert.show();

        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(SettingsActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            return;
        }
        makeActiveStatusOffline();
    }

    private void makeActiveStatusOffline() {
        mProgressDialog.setMessage("Logging out...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE_ACTIVE_STATUS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                logoutBaker();
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(SettingsActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(SettingsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ID, SharedPrefSingleton.getInstance(SettingsActivity.this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                        params.put(ApiConstants.PARAM_ACTIVE_STATUS, ApiConstants.PARAM_VALUE_OFFLINE);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(SettingsActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    private void logoutBaker() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_LOGOUT_BAKER, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                SharedPrefSingleton.getInstance(SettingsActivity.this).clearBakerData();
                                ActivityCompat.finishAffinity(SettingsActivity.this); // Clear the previous activities stack
                                startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(SettingsActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(SettingsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ID, SharedPrefSingleton.getInstance(SettingsActivity.this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(SettingsActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }
}
