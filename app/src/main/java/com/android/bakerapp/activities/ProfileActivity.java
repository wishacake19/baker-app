package com.android.bakerapp.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.bakerapp.R;
import com.android.bakerapp.fragments.AboutFragment;
import com.android.bakerapp.fragments.ReviewsFragment;
import com.android.bakerapp.fragments.Top3CakesFragment;

public class ProfileActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    public static TabLayout sTabLayoutProfile;
    private ViewPager mViewPagerProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        ProfileViewPagerAdapter profileViewPagerAdapter = new ProfileViewPagerAdapter(getSupportFragmentManager());
        mViewPagerProfile.setAdapter(profileViewPagerAdapter);
        mViewPagerProfile.setOffscreenPageLimit(3);
        mViewPagerProfile.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(sTabLayoutProfile));
        sTabLayoutProfile.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPagerProfile));
    }

    @Override
    public void onBackPressed() {
        if (mViewPagerProfile.getCurrentItem() == 1 || mViewPagerProfile.getCurrentItem() == 2) {
            mViewPagerProfile.setCurrentItem(0);
            return;
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        sTabLayoutProfile = (TabLayout) findViewById(R.id.tl_profile);
        mViewPagerProfile = (ViewPager) findViewById(R.id.vp_profile);
    }

    private class ProfileViewPagerAdapter extends FragmentPagerAdapter {

        public ProfileViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = new AboutFragment();
            } else if (position == 1) {
                fragment = new Top3CakesFragment();
            } else if (position == 2) {
                fragment = new ReviewsFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
