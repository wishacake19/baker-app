package com.android.bakerapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.ErrorTextWatcher;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.PermissionUtils;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class CreateAccountActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEditTextFirstName, mEditTextLastName, mEditTextEmail, mEditTextMobileNumber, mEditTextCnic,
            mEditTextPin, mEditTextAddress;
    private Button mButtonContinue;
    private TextView mTextViewHelperFirstName, mTextViewHelperLastName, mTextViewHelperEmail, mTextViewHelperMobileNumber,
            mTextViewHelperCnic, mTextViewHelperPin, mTextViewHelperAddress, mTextViewVerify, mTextViewTermsAndPrivacy,
            mTextViewHelperTermsAndPrivacy, mTextViewAlreadyHaveAnAccount;
    private CheckBox mCheckBoxTermsAndPrivacy;

    private String mFirstName, mLastName, mEmail, mMobileNumber, mCnic, mPin, mLocationId, mLocationName,
            mLocationAddress, mLocationLatitude, mLocationLongitude;
    private boolean mIsMobileNumberVerified = false;

    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = CreateAccountActivity.class.getSimpleName();
    private static final int VERIFY_MOBILE_NUMBER_REQUEST_CODE = 21;
    private static final int PLACE_PICKER_REQUEST_CODE = 14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        initViews();

        getSupportActionBar().hide();

        mEditTextFirstName.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperFirstName));
        mEditTextLastName.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperLastName));
        mEditTextEmail.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperEmail));
        mEditTextMobileNumber.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperMobileNumber));
        mEditTextCnic.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperCnic));
        mEditTextPin.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperPin));
        mEditTextAddress.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperAddress));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        mEditTextMobileNumber.setText(Constants.MOBILE_NUMBER_PAK_PREFIX);
        Selection.setSelection(mEditTextMobileNumber.getText(), mEditTextMobileNumber.getText().length());

        mEditTextMobileNumber.addTextChangedListener(new TextWatcher() {
            String text;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                text = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().startsWith(Constants.MOBILE_NUMBER_PAK_PREFIX)) {
                    mEditTextMobileNumber.setText(text);
                    Selection.setSelection(mEditTextMobileNumber.getText(), mEditTextMobileNumber.getText().length());
                }
            }
        });

//        mEditTextAddress.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    if (mEditTextAddress.getCompoundDrawables()[2] != null) {
//                        if (mEditTextAddress.getCompoundDrawables()[2].getConstantState().equals(ContextCompat.getDrawable(CreateAccountActivity.this, R.drawable.ic_address).getConstantState())) {
//                            if (event.getX() >= (mEditTextAddress.getRight() - mEditTextAddress.getLeft() - mEditTextAddress.getCompoundDrawables()[2].getBounds().width())) {
//                                if (PermissionUtils.checkLocationPermission(CreateAccountActivity.this)) {
//                                    openPlacePickerWidget();
//                                }
//                            }
//                        }
//                    }
//                }
//                return false;
//            }
//        });

        mCheckBoxTermsAndPrivacy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mTextViewHelperTermsAndPrivacy.setVisibility(View.GONE);
                mTextViewHelperTermsAndPrivacy.setText("");
            }
        });

        mTextViewTermsAndPrivacy.setText(Html.fromHtml("I have read and agree to the wishacake's " +
                "<br><font color='#2196F3'><a href='http://feedpakistan.com/wishacake/terms-of-service.html'><u>Terms of service</u></font>" +
                " & <font color='#2196F3'><a href='http://feedpakistan.com/wishacake/privacy-policy.html'><u>Privacy policy</u></font>."));
        mTextViewTermsAndPrivacy.setMovementMethod(LinkMovementMethod.getInstance());

        mTextViewVerify.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mTextViewAlreadyHaveAnAccount.setOnClickListener(this);
        mEditTextAddress.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view == mTextViewVerify) {
            onClickVerify();
        }
        if (view == mButtonContinue) {
            onClickContinue();
        }
        if (view == mTextViewAlreadyHaveAnAccount) {
            finish();
        }
        if (view == mEditTextAddress) {
            if (PermissionUtils.checkLocationPermission(CreateAccountActivity.this)) {
                openPlacePickerWidget();
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_LOCATION) {
            if (grantResults.length > 0) {
                boolean locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (locationPermission) {
                    openPlacePickerWidget();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && requestCode == PLACE_PICKER_REQUEST_CODE) {
            Place place = PlacePicker.getPlace(this, data);
            mLocationId = place.getId();
            mLocationName = place.getName().toString();
            mLocationAddress = place.getAddress().toString();
            mLocationLatitude = String.valueOf(place.getLatLng().latitude);
            mLocationLongitude = String.valueOf(place.getLatLng().longitude);
            mEditTextAddress.setText(place.getName().toString() + "\n" + place.getAddress().toString());
        }
        if (requestCode == VERIFY_MOBILE_NUMBER_REQUEST_CODE && resultCode == RESULT_OK) {
            AccountKitLoginResult result = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if (result.getError() != null) {
                return;
            } else if (result.wasCancelled()) {
                return;
            } else {
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        mIsMobileNumberVerified = true;
                        mMobileNumber = String.valueOf(account.getPhoneNumber());
                        mTextViewVerify.setVisibility(View.GONE);
                        mEditTextMobileNumber.setText(mMobileNumber);
                        Utils.hideError(mTextViewHelperMobileNumber);
                        mEditTextMobileNumber.setEnabled(false);
                        mEditTextCnic.requestFocus();
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {
                    }
                });

            }
        }
    }

    private void initViews() {
        mEditTextFirstName = (EditText) findViewById(R.id.et_first_name);
        mEditTextLastName = (EditText) findViewById(R.id.et_last_name);
        mEditTextEmail = (EditText) findViewById(R.id.et_email);
        mEditTextMobileNumber = (EditText) findViewById(R.id.et_mobile_number);
        mEditTextCnic = (EditText) findViewById(R.id.et_cnic);
        mEditTextPin = (EditText) findViewById(R.id.et_pin);
        mEditTextAddress = (EditText) findViewById(R.id.et_address);
        mTextViewHelperFirstName = (TextView) findViewById(R.id.tv_helper_first_name);
        mTextViewHelperLastName = (TextView) findViewById(R.id.tv_helper_last_name);
        mTextViewHelperEmail = (TextView) findViewById(R.id.tv_helper_email);
        mTextViewHelperMobileNumber = (TextView) findViewById(R.id.tv_helper_mobile_number);
        mTextViewHelperCnic = (TextView) findViewById(R.id.tv_helper_cnic);
        mTextViewHelperPin = (TextView) findViewById(R.id.tv_helper_pin);
        mTextViewHelperAddress = (TextView) findViewById(R.id.tv_helper_address);
        mTextViewTermsAndPrivacy = (TextView) findViewById(R.id.tv_terms_and_privacy);
        mTextViewHelperTermsAndPrivacy = (TextView) findViewById(R.id.tv_helper_terms_and_privacy);
        mCheckBoxTermsAndPrivacy = (CheckBox) findViewById(R.id.cb_terms_and_privacy);
        mButtonContinue = (Button) findViewById(R.id.btn_continue);
        mTextViewVerify = (TextView) findViewById(R.id.tv_verify);
        mTextViewAlreadyHaveAnAccount = (TextView) findViewById(R.id.tv_already_have_an_account);
    }

    private void printHashKey() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.android.bakerapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : packageInfo.signatures) {
                try {
                    MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                    messageDigest.update(signature.toByteArray());
                    Log.i(LOG_TAG + " HASH KEY :",
                            Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean isValid() {
        boolean flag = false;

        mFirstName = mEditTextFirstName.getText().toString().trim();
        mLastName = mEditTextLastName.getText().toString().trim();
        mEmail = mEditTextEmail.getText().toString().trim();
        mMobileNumber = mEditTextMobileNumber.getText().toString().trim();
        mPin = mEditTextPin.getText().toString().trim();
        mCnic = mEditTextCnic.getText().toString().trim();

        if (mFirstName.isEmpty()) {
            Utils.showError(mEditTextFirstName, "First name is required.", mTextViewHelperFirstName);
        } else if (Utils.isNameValid(mFirstName) == false) {
            Utils.showError(mEditTextFirstName, "Please enter a valid first name.", mTextViewHelperFirstName);
        } else if (mLastName.isEmpty()) {
            Utils.showError(mEditTextLastName, "Last name is required.", mTextViewHelperLastName);
        } else if (Utils.isNameValid(mLastName) == false) {
            Utils.showError(mEditTextLastName, "Please enter a valid last name.", mTextViewHelperLastName);
        } else if (mEmail.isEmpty()) {
            Utils.showError(mEditTextEmail, "Email address is required.", mTextViewHelperEmail);
        } else if (Utils.isEmailValid(mEmail) == false) {
            Utils.showError(mEditTextEmail, "Please enter a valid email address.", mTextViewHelperEmail);
        } else if (mMobileNumber.isEmpty()) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is required.", mTextViewHelperMobileNumber);
        } else if (Utils.isMobileNumberValid(mMobileNumber) == false) {
            Utils.showError(mEditTextMobileNumber, "Please enter a valid mobile number.", mTextViewHelperMobileNumber);
        } else if (!mIsMobileNumberVerified) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is not verified.", mTextViewHelperMobileNumber);
        } else if (mCnic.isEmpty()) {
            Utils.showError(mEditTextCnic, "CNIC is required.", mTextViewHelperCnic);
        } else if (Utils.isCnicValid(mCnic) == false) {
            Utils.showError(mEditTextCnic, "Please enter a valid CNIC.", mTextViewHelperCnic);
        } else if (mPin.isEmpty()) {
            Utils.showError(mEditTextPin, "PIN is required.", mTextViewHelperPin);
        }

        else if (Utils.isPinValid(mPin) == false) {
            Utils.showError(mEditTextPin, "Please enter a valid PIN.", mTextViewHelperPin);
        } else if (mEditTextAddress.getText().toString().trim().isEmpty()) {
            Utils.showError(mEditTextAddress, "Address is required.", mTextViewHelperAddress);
        } else if (!mCheckBoxTermsAndPrivacy.isChecked()) {
            mTextViewHelperTermsAndPrivacy.setVisibility(View.VISIBLE);
            mTextViewHelperTermsAndPrivacy.setText("You must accept the terms of service & privacy and policy to continue.");
        } else {
            flag = true;
        }
        return flag;
    }

    private void openPlacePickerWidget() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("PK")
                .build();
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(typeFilter)
                    .build(CreateAccountActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void onClickVerify() {
        mMobileNumber = mEditTextMobileNumber.getText().toString().trim();
        if (mMobileNumber.isEmpty()) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is required.", mTextViewHelperMobileNumber);
        } else if (Utils.isMobileNumberValid(mMobileNumber) == false) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is invalid.", mTextViewHelperMobileNumber);
        } else {
            if (!Utils.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                return;
            }
            verify();
        }
    }

    private void verify() {
        Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder accountKitConfigurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);  // Use token when Yes 'Enable Client Access Token Flow'
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, accountKitConfigurationBuilder.build());
        startActivityForResult(intent, VERIFY_MOBILE_NUMBER_REQUEST_CODE);
    }

    private void onClickContinue() {
        if (isValid()) {
            if (!Utils.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                return;
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    validate();
                    return null;
                }
            }.execute();
        }
    }

    private void validate() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_VALIDATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        SharedPrefSingleton.getInstance(CreateAccountActivity.this).saveBakerData(null, null,
                                mFirstName, mLastName, mEmail.toLowerCase(), mMobileNumber, mCnic, mPin, mLocationId,
                                mLocationName, mLocationAddress, mLocationLatitude, mLocationLongitude,
                                null, null, null, null, null, null);

                        Intent intent = new Intent(CreateAccountActivity.this, PhotosActivity.class);
                        intent.putExtra(Constants.EXTRA_KEY_IS_PHOTOS_OPENED_AFTER_SPLASH, false);
                        startActivity(intent);
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("0")) {
                            Utils.showError(mEditTextEmail, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), mTextViewHelperEmail);
                        } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("2")) {
                            mEditTextMobileNumber.setEnabled(true);
                            mEditTextMobileNumber.requestFocus();
                            Selection.setSelection(mEditTextMobileNumber.getText(), mEditTextMobileNumber.getText().length());
                            mTextViewVerify.setVisibility(View.VISIBLE);
                            mIsMobileNumberVerified = false;
                            Utils.showError(mEditTextMobileNumber, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), mTextViewHelperMobileNumber);
                        } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("3")) {
                            Utils.showError(mEditTextCnic, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), mTextViewHelperCnic);
                        }
//                        else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
//                            Toast.makeText(CreateAccountActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
//                                    Toast.LENGTH_SHORT).show();
//                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(CreateAccountActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_EMAIL, mEmail.toLowerCase());
                params.put(ApiConstants.PARAM_MOBILE_NUMBER, mMobileNumber);
                params.put(ApiConstants.PARAM_CNIC, Utils.getFormattedCNIC(mCnic));
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
