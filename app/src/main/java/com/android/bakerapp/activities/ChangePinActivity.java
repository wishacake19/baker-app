package com.android.bakerapp.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.ErrorTextWatcher;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePinActivity extends AppCompatActivity {

    private EditText mEditTextCurrentPin, mEditTextNewPin, mEditTextConfirmPin;
    private TextView mTextViewHelperCurrentPin, mTextViewHelperNewPin, mTextViewHelperConfirmPin;
    private Button mButtonChangePin;

    private String mCurrentPin, mNewPin, mConfirmPin;

    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = ChangePinActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        initViews();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        mEditTextCurrentPin.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperCurrentPin));
        mEditTextNewPin.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperNewPin));
        mEditTextConfirmPin.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperConfirmPin));

        mButtonChangePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    onClickChangePin();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initViews() {
        mEditTextCurrentPin = (EditText) findViewById(R.id.et_current_pin);
        mEditTextNewPin = (EditText) findViewById(R.id.et_new_pin);
        mEditTextConfirmPin = (EditText) findViewById(R.id.et_confirm_pin);
        mTextViewHelperCurrentPin = (TextView) findViewById(R.id.tv_helper_current_pin);
        mTextViewHelperNewPin = (TextView) findViewById(R.id.tv_helper_new_pin);
        mTextViewHelperConfirmPin = (TextView) findViewById(R.id.tv_helper_confirm_pin);
        mButtonChangePin = (Button) findViewById(R.id.btn_change_pin);
    }

    private boolean isValid() {
        boolean flag = false;

        mCurrentPin = mEditTextCurrentPin.getText().toString().trim();
        mNewPin = mEditTextNewPin.getText().toString().trim();
        mConfirmPin = mEditTextConfirmPin.getText().toString().trim();

        String actualCurrentPin = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_PIN);

        if (mCurrentPin.isEmpty()) {
            Utils.showError(mEditTextCurrentPin, "Current PIN is required.", mTextViewHelperCurrentPin);
        } else if (Utils.isPinValid(mCurrentPin) == false) {
            Utils.showError(mEditTextCurrentPin, "Please enter a valid PIN.", mTextViewHelperCurrentPin);
        } else if (!mCurrentPin.equals(actualCurrentPin)) {
            Utils.showError(mEditTextCurrentPin, "Wrong PIN.", mTextViewHelperCurrentPin);
        } else if (mNewPin.isEmpty()) {
            Utils.showError(mEditTextNewPin, "New PIN is required.", mTextViewHelperNewPin);
        } else if (Utils.isPinValid(mNewPin) == false) {
            Utils.showError(mEditTextNewPin, "Please enter a valid PIN.", mTextViewHelperNewPin);
        } else if (mCurrentPin.equals(mNewPin)) {
            Utils.showError(mEditTextNewPin, "New PIN must be different the current one.", mTextViewHelperNewPin);
        } else if (mConfirmPin.isEmpty()) {
            Utils.showError(mEditTextConfirmPin, "Confirm PIN is required.", mTextViewHelperConfirmPin);
        } else if (Utils.isPinValid(mConfirmPin) == false) {
            Utils.showError(mEditTextConfirmPin, "Please enter a valid PIN.", mTextViewHelperConfirmPin);
        } else if (!mConfirmPin.equals(mNewPin)) {
            Utils.showError(mEditTextConfirmPin, "PIN does not match.", mTextViewHelperConfirmPin);
        } else {
            flag = true;
        }
        return flag;
    }

    private void onClickChangePin() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            return;
        }
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                changePin();
                return null;
            }
        }.execute();
    }

    private void changePin() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_CHANGE_PIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                        String image = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_IMAGE);
                        String firstName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                        String lastName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                        String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                        String mobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                        String cnic = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_CNIC);
                        String pin = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PIN);
                        String locationId = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_ID);
                        String locationName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_NAME);
                        String locationAddress = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                        String locationLatitude = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_LATITUDE);
                        String locationLongitude = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_LONGITUDE);
                        String sliderImage1 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE1);
                        String sliderImage2 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE2);
                        String sliderImage3 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE3);
                        String rating = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_RATING))
                                ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_RATING);
                        String activeStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACTIVE_STATUS);
                        String bakerEarning = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_BAKER_EARNING);

                        if (activeStatus.equals("1")) {
                            activeStatus = ApiConstants.PARAM_VALUE_ONLINE;
                        } else if (activeStatus.equals("0")) {
                            activeStatus = ApiConstants.PARAM_VALUE_OFFLINE;
                        }

                        SharedPrefSingleton.getInstance(ChangePinActivity.this).saveBakerData(id, image, firstName, lastName, email,
                                mobileNumber, cnic, pin, locationId, locationName, locationAddress, locationLatitude,
                                locationLongitude, sliderImage1, sliderImage2, sliderImage3, rating, bakerEarning, activeStatus);

                        Toast.makeText(ChangePinActivity.this, "PIN changed successfully.", Toast.LENGTH_SHORT).show();

                        finish();
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                            Toast.makeText(ChangePinActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(ChangePinActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_ID, SharedPrefSingleton.getInstance(ChangePinActivity.this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                params.put(ApiConstants.PARAM_PIN, mConfirmPin);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
