package com.android.bakerapp.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.adapters.HelpAdapter;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.HelpItem;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.utilities.PermissionUtils;
import com.android.bakerapp.utilities.RealPathUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HelpActivity extends AppCompatActivity {

    private LinearLayout mLinearLayoutHeaderViews;
    private EditText mEditTextDescription, mEditTextDescriptionJellyBean;
    private TextView mTextViewAttachAnImage;
    private Button mButtonSubmit;
    private List<HelpItem> mHelpItems;
    private HelpAdapter mHelpAdapter;
    private FrameLayout mFrameLayoutDescription, mFrameLayoutSubmit;

    public static FrameLayout sFrameLayoutUploadImage;
    public static Button sButtonUploadAnImage;
    public static RecyclerView sRecyclerViewHelpItems;

    private static final int REQUEST_CODE_GALLERY = 01;
    private static final int REQUEST_CODE_CAMERA = 02;
    private static final String LOG_TAG = HelpActivity.class.getSimpleName();

    private static String sAppName;

    // For SDK >= 24
    private static String sCurrentPhotoPath;
    private static File sStorageDirectory;

    // For SDK < 24
    private Uri mPhotoUri;
    private static String sCurrentPhotoName;

    public static int sHelpImageCount = 0;

    @SuppressLint("ClickableViewAccessibility")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        initViews();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.KEY_IS_HELP_OPENED_FOR_REPORTING)) {
                if (extras.getBoolean(Constants.KEY_IS_HELP_OPENED_FOR_REPORTING)) {
                    getSupportActionBar().setTitle(getString(R.string.order_details_report_a_problem));
                }
                else {
                    String firstName = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_FIRST_NAME);
                    mEditTextDescription.setHint("Hi " + firstName + ", How can we help you?");
                    mEditTextDescriptionJellyBean.setHint("Hi " + firstName + ", How can we help you?");
                }
            }
            else {
                String firstName = SharedPrefSingleton.getInstance(this).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_FIRST_NAME);
                mEditTextDescription.setHint("Hi " + firstName + ", How can we help you?");
                mEditTextDescriptionJellyBean.setHint("Hi " + firstName + ", How can we help you?");
            }
        }

        mHelpItems = new ArrayList<>();
        mHelpAdapter = new HelpAdapter(HelpActivity.this, mHelpItems);
        sRecyclerViewHelpItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        sRecyclerViewHelpItems.setAdapter(mHelpAdapter);

        sAppName = getString(R.string.app_name);

        sStorageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        // For inside scrollbar of input field
        mEditTextDescription.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_description) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mEditTextDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    if (hasFocus) {
                        mFrameLayoutDescription.setBackground(ContextCompat.getDrawable(HelpActivity.this, R.drawable.outlined_input_border_focused));
                    } else {
                        mFrameLayoutDescription.setBackground(ContextCompat.getDrawable(HelpActivity.this, R.drawable.outlined_input_border_unfocused));
                    }
                }
            }
        });

        mEditTextDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    if (mEditTextDescription.getText().toString().trim().isEmpty()) {
                        mFrameLayoutSubmit.setBackground(ContextCompat.getDrawable(HelpActivity.this, R.drawable.contained_normal_button_disabled));
                        mButtonSubmit.setEnabled(false);
                    } else {
                        mFrameLayoutSubmit.setBackground(ContextCompat.getDrawable(HelpActivity.this, R.drawable.contained_normal_button));
                        mButtonSubmit.setEnabled(true);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mEditTextDescriptionJellyBean.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_description_jellybean) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mEditTextDescriptionJellyBean.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    if (mEditTextDescriptionJellyBean.getText().toString().trim().isEmpty()) {
                        mFrameLayoutSubmit.setBackground(ContextCompat.getDrawable(HelpActivity.this, R.drawable.contained_normal_button_disabled));
                        mButtonSubmit.setEnabled(false);
                    } else {
                        mFrameLayoutSubmit.setBackground(ContextCompat.getDrawable(HelpActivity.this, R.drawable.contained_normal_button));
                        mButtonSubmit.setEnabled(true);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        sButtonUploadAnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(HelpActivity.this);
                builder.setTitle("");
                builder.setMessage(getString(R.string.help_upload_image_dialog_msg));
                builder.setCancelable(true);

                builder.setPositiveButton(getString(R.string.help_camera_alert_dialog_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (PermissionUtils.checkCameraPermission(HelpActivity.this)) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                // Create the File where the photo should go
                                File photoFile = null;
                                try {
                                    photoFile = createImageFileForSDK24AndAbove();
                                } catch (IOException ex) {
                                    // Error occurred while creating the File
                                    Log.e(LOG_TAG, "IOException || " + ex.getMessage());
                                }
                                // Continue only if the File was successfully created
                                if (photoFile != null) {
                                    Uri photoURI = FileProvider.getUriForFile(HelpActivity.this,
                                            getPackageName() + ".fileprovider",
                                            photoFile);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                    dialog.dismiss();
                                    startActivityForResult(intent, REQUEST_CODE_CAMERA);
                                }
                            } else {
                                mPhotoUri = Uri.fromFile(createImageFileForSDKBelow24());
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                                startActivityForResult(intent, REQUEST_CODE_CAMERA);
                            }
                        }
                        dialog.dismiss();
                    }
                });

                builder.setNeutralButton(getString(R.string.help_gallery_alert_dialog_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (PermissionUtils.checkGalleryPermission(HelpActivity.this)) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            dialog.dismiss();
                            startActivityForResult(Intent.createChooser(intent, "Select picture"), REQUEST_CODE_GALLERY);
                        }
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HelpActivity.this, "Not implemented yet.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean writeStoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeStoragePermission) {
                    openGallery();
                }
            }
        }
        if (requestCode == PermissionUtils.REQUEST_CODE_CAMERA_OR_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (cameraPermission) {
                    if (!PermissionUtils.checkGalleryPermission(HelpActivity.this)) {
                        boolean writeStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        if (writeStoragePermission) {
                            openCamera();
                        }
                    }
                    openCamera();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                String imagePath = "";
                if (Build.VERSION.SDK_INT < 11) {
                    imagePath = RealPathUtils.getRealPathFromURI_BelowSDK11(this, uri);
                } else if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT <= 18) {
                    imagePath = RealPathUtils.getRealPathFromURI_SDK11to18(this, uri);
                } else {
                    imagePath = RealPathUtils.getRealPathFromURI_SDK19(this, uri);
                }
                String fileName;
                if (!imagePath.isEmpty()) {
                    fileName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
                } else {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    fileName = "IMG_" + timeStamp + ".jpg";
                }

                Log.v(LOG_TAG + "Image Gallery || ", imagePath);
                Log.v(LOG_TAG + "Image Gallery || ", fileName);

                sRecyclerViewHelpItems.setVisibility(View.VISIBLE);
                mHelpItems.add(new HelpItem(uri, fileName));
                mHelpAdapter.notifyDataSetChanged();
                sHelpImageCount++;
            } else {
                Log.v(LOG_TAG + "Image Gallery || ", "Data is null");
            }
        }
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
                        Uri uri = Uri.parse(this.getPathFromBitmap(imageBitmap));
                        String fileName = sCurrentPhotoPath.substring(sCurrentPhotoPath.lastIndexOf("/") + 1);

                        Log.v(LOG_TAG + "Image Camera || ", sCurrentPhotoPath);
                        Log.v(LOG_TAG + "Image Camera || ", fileName);

                        sRecyclerViewHelpItems.setVisibility(View.VISIBLE);
                        mHelpItems.add(new HelpItem(uri, fileName));
                        mHelpAdapter.notifyDataSetChanged();
                        sHelpImageCount++;
                    } else {
                        Toast.makeText(this, "An unknown error has occurred while saving the image. Please try again.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.v(LOG_TAG + "Image Camera || ", "Data is null");
                }
            } else {
                Log.v(LOG_TAG + "Image Camera || ", createImageFileForSDKBelow24().getAbsolutePath());
                Log.v(LOG_TAG + "Image Camera || ", sCurrentPhotoName);

                sRecyclerViewHelpItems.setVisibility(View.VISIBLE);
                mHelpItems.add(new HelpItem(mPhotoUri, sCurrentPhotoName));
                mHelpAdapter.notifyDataSetChanged();
                sHelpImageCount++;
            }
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mEditTextDescriptionJellyBean = (EditText) findViewById(R.id.et_description_jellybean);
        mEditTextDescription = (EditText) findViewById(R.id.et_description);
        mTextViewAttachAnImage = (TextView) findViewById(R.id.tv_attach_an_image);
        sFrameLayoutUploadImage = (FrameLayout) findViewById(R.id.frame_layout_upload_image);
        sButtonUploadAnImage = (Button) findViewById(R.id.btn_upload_an_image);
        sRecyclerViewHelpItems = (RecyclerView) findViewById(R.id.rv_help_items);
        mButtonSubmit = (Button) findViewById(R.id.btn_submit);
        mFrameLayoutDescription = (FrameLayout) findViewById(R.id.frame_layout_description);
        mFrameLayoutSubmit = (FrameLayout) findViewById(R.id.frame_layout_submit);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mFrameLayoutDescription.setVisibility(View.GONE);
            mEditTextDescriptionJellyBean.setVisibility(View.VISIBLE);
        }
        mTextViewAttachAnImage.setText(Html.fromHtml("<font color='#212121'>Attach an image</font>"
                + " <font color='#757575'>(optional)</font>"));
    }

    private String getPathFromBitmap(Bitmap imageBitmap) {
        return MediaStore.Images.Media.insertImage(getContentResolver(), imageBitmap, "title", null);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture"), REQUEST_CODE_GALLERY);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFileForSDK24AndAbove();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(LOG_TAG, "IOException || " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(HelpActivity.this,
                        "com.android.wishacake.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
            }
        } else {
            mPhotoUri = Uri.fromFile(createImageFileForSDKBelow24());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
            startActivityForResult(intent, REQUEST_CODE_CAMERA);
        }
    }

    private static File createImageFileForSDK24AndAbove() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + timeStamp;
        File imageFile = File.createTempFile(fileName, ".jpg", sStorageDirectory);
        sCurrentPhotoPath = imageFile.getAbsolutePath();
        return imageFile;
    }

    private static File createImageFileForSDKBelow24() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), sAppName);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        sCurrentPhotoName = "IMG_" + timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + sCurrentPhotoName);
    }
}
