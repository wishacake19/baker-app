package com.android.bakerapp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.ErrorTextWatcher;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.android.bakerapp.helpers.Constants.EXTRA_KEY_ORDER_DELIVERY_DATE_AND_TIME;

public class AcceptRejectActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEditTextSubtotal, mEditTextDeliveryCharges, mEditTextTotalAmount, mEditTextReason, mEditTextReasonJellyBean;
    private TextView mTextViewHelperSubtotal, mTextViewHelperDeliveryCharges;
    private Button mButtonAcceptReAccept, mButtonReject;
    private LinearLayout mLinearLayoutAccept, mLinearLayoutReject, mLinearLayoutReason;

    private int mCurrentActivityMode = 0;
    private String mOrderId, mUserId, mBakerId, mSubtotalStr, mDeliveryChargesStr, mTotalAmountStr, mRejectedReason = "null";
    private double mSubtotal, mDeliveryCharges, mTotalAmount;

    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = AcceptRejectActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_reject);

        initViews();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        mEditTextSubtotal.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperSubtotal));
        mEditTextDeliveryCharges.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperDeliveryCharges));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            getSupportActionBar().setTitle(extras.getString(EXTRA_KEY_ORDER_DELIVERY_DATE_AND_TIME));
            mCurrentActivityMode = extras.getInt(Constants.EXTRA_KEY_ORDER_ACTIVITY_MODE);
            mOrderId = extras.getString(Constants.EXTRA_KEY_ORDER_ID);
            mUserId = extras.getString(Constants.EXTRA_KEY_ORDER_USER_ID);
            mBakerId = extras.getString(Constants.EXTRA_KEY_ORDER_BAKER_ID);

            if (mCurrentActivityMode == Constants.ACTIVITY_MODE_RE_ACCEPT) {
                mButtonAcceptReAccept.setText("Re-accept");
                mSubtotal = extras.getDouble(Constants.EXTRA_KEY_ORDER_SUBTOTAL);
                mDeliveryCharges = extras.getDouble(Constants.EXTRA_KEY_ORDER_DELIVERY_CHARGES);
                mTotalAmount = extras.getDouble(Constants.EXTRA_KEY_ORDER_TOTAL_AMOUNT);
                mSubtotalStr = String.format("%.0f", mSubtotal);
                mDeliveryChargesStr = String.format("%.0f", mDeliveryCharges);
                mTotalAmountStr = String.format("%.0f", mTotalAmount);
                mEditTextSubtotal.setText(mSubtotalStr);
                mEditTextDeliveryCharges.setText(mDeliveryChargesStr);
                mEditTextTotalAmount.setText("Total amount – PKR " + mTotalAmountStr);
            } else if (mCurrentActivityMode == Constants.ACTIVITY_MODE_REJECT) {
                mLinearLayoutReject.setVisibility(View.VISIBLE);
                mLinearLayoutAccept.setVisibility(View.GONE);

                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    mLinearLayoutReason.setVisibility(View.GONE);
                    mEditTextReasonJellyBean.setVisibility(View.VISIBLE);
                }
            }
        }

        // For inside scrollbar of input field
        mEditTextReason.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_reason) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mEditTextReasonJellyBean.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_reason_jellybean) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mEditTextSubtotal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    String subtotal = mEditTextSubtotal.getText().toString().trim();
                    String deliveryCharges = mEditTextDeliveryCharges.getText().toString().trim();

                    if (subtotal.equals("") && deliveryCharges.equals("")) {
                        mEditTextTotalAmount.setText("Total amount – PKR 0");
                    } else {
                        int total = 0;
                        if (!subtotal.equals("")) {
                            total += Integer.parseInt(subtotal);
                        }
                        if (!deliveryCharges.equals("")) {
                            total += Integer.parseInt(deliveryCharges);
                        }
                        mEditTextTotalAmount.setText("Total amount – PKR " + total);
                    }
                } catch (Exception e) {
                    if (e != null) {
                        Log.e(LOG_TAG, e.getMessage());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mEditTextDeliveryCharges.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    String subtotal = mEditTextSubtotal.getText().toString().trim();
                    String deliveryCharges = mEditTextDeliveryCharges.getText().toString().trim();

                    if (subtotal.equals("") && deliveryCharges.equals("")) {
                        mEditTextTotalAmount.setText("Total amount – PKR 0");
                    } else {
                        int total = 0;
                        if (!deliveryCharges.equals("")) {
                            total += Integer.parseInt(deliveryCharges);
                        }
                        if (!subtotal.equals("")) {
                            total += Integer.parseInt(subtotal);
                        }
                        mEditTextTotalAmount.setText("Total amount – PKR " + total);
                    }
                } catch (Exception e) {
                    if (e != null) {
                        Log.e(LOG_TAG, e.getMessage());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mButtonAcceptReAccept.setOnClickListener(this);
        mButtonReject.setOnClickListener(this);
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view == mButtonAcceptReAccept) {
            if (isAcceptDataValid()) {
                if (Utils.isStringEmptyOrNull(mDeliveryChargesStr)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Delivery charges confirmation");
                    builder.setMessage("You haven't set any delivery charges for this order. By default it will be set to 0 PKR. Are you sure want to proceed?");

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                        }
                    });

                    builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            checkIfAcceptOrReAccept();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    checkIfAcceptOrReAccept();
                }
            }
        }
        if (view == mButtonReject) {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mRejectedReason = mEditTextReasonJellyBean.getText().toString().trim();
            } else {
                mRejectedReason = mEditTextReason.getText().toString().trim();
            }
            if (Utils.isStringEmptyOrNull(mRejectedReason)) {
                mRejectedReason = "null";
            }
            mSubtotalStr = "0";
            mDeliveryChargesStr = "0";
            mTotalAmountStr = "0";
            showConfirmAlertDialog("Reject confirmation", "Are you sure want to reject this order?", "Reject", ApiConstants.PARAM_VALUE_REJECTED);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mEditTextSubtotal = (EditText) findViewById(R.id.et_subtotal);
        mEditTextDeliveryCharges = (EditText) findViewById(R.id.et_delivery_charges);
        mEditTextTotalAmount = (EditText) findViewById(R.id.et_total_amount);
        mEditTextReason = (EditText) findViewById(R.id.et_reason);
        mEditTextReasonJellyBean = (EditText) findViewById(R.id.et_reason_jellybean);
        mTextViewHelperSubtotal = (TextView) findViewById(R.id.tv_helper_subtotal);
        mTextViewHelperDeliveryCharges = (TextView) findViewById(R.id.tv_helper_delivery_charges);
        mButtonAcceptReAccept = (Button) findViewById(R.id.btn_accept_re_accept);
        mButtonReject = (Button) findViewById(R.id.btn_reject);
        mLinearLayoutAccept = (LinearLayout) findViewById(R.id.linear_layout_accept);
        mLinearLayoutReject = (LinearLayout) findViewById(R.id.linear_layout_reject);
        mLinearLayoutReason = (LinearLayout) findViewById(R.id.linear_layout_reason);
    }

    private boolean isAcceptDataValid() {
        boolean flag = false;

        mSubtotalStr = mEditTextSubtotal.getText().toString().trim();
        mDeliveryChargesStr = mEditTextDeliveryCharges.getText().toString().trim();

        if (mSubtotalStr.isEmpty()) {
            Utils.showError(mEditTextSubtotal, "Subtotal is required.", mTextViewHelperSubtotal);
        } else {
            mSubtotal = Double.parseDouble(mSubtotalStr);
            mDeliveryCharges = Utils.isStringEmptyOrNull(mDeliveryChargesStr) ? 0 : Double.parseDouble(mDeliveryChargesStr);
            mTotalAmount = Double.parseDouble(mEditTextTotalAmount.getText().toString().substring("Total amount – PKR ".length()));
            flag = true;
        }
        return flag;
    }

    private void checkIfAcceptOrReAccept() {
        mSubtotalStr = String.format("%.0f", mSubtotal);
        mDeliveryChargesStr = String.format("%.0f", mDeliveryCharges);
        mTotalAmountStr = String.format("%.0f", mTotalAmount);
        if (mCurrentActivityMode == Constants.ACTIVITY_MODE_ACCEPT) {
            showConfirmAlertDialog("Accept confirmation", "Are you sure want to accept this order?", "Accept", ApiConstants.PARAM_VALUE_ACCEPTED);
        } else if (mCurrentActivityMode == Constants.ACTIVITY_MODE_RE_ACCEPT) {
            showConfirmAlertDialog("Re-accept confirmation", "Are you sure want to re-accept this order with the new price?", "Re-accept", ApiConstants.PARAM_VALUE_RE_ACCEPT);
        }
    }

    private void showConfirmAlertDialog(String title, String msg, String positiveButtonText, final String statusParamValue) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        builder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                logApiParamValues(statusParamValue);
                updateOrderStatus(statusParamValue);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void logApiParamValues(String statusParamValue) {
        Log.d(LOG_TAG + "-> " + ApiConstants.PARAM_ORDER_ID, mOrderId);
        Log.d(LOG_TAG + "-> " + ApiConstants.PARAM_USER_ID, mUserId);
        Log.d(LOG_TAG + "-> " + ApiConstants.PARAM_BAKER_ID, mBakerId);
        Log.d(LOG_TAG + "-> " + ApiConstants.PARAM_SUBTOTAL, mSubtotalStr);
        Log.d(LOG_TAG + "-> " + ApiConstants.PARAM_DELIVERY_CHARGES, mDeliveryChargesStr);
        Log.d(LOG_TAG + "-> " + ApiConstants.PARAM_TOTAL_AMOUNT, mTotalAmountStr);
        Log.d(LOG_TAG + "-> " + ApiConstants.PARAM_REJECTED_REASON, mRejectedReason);
        Log.d(LOG_TAG + "-> " + ApiConstants.PARAM_STATUS, statusParamValue);
    }

    private void updateOrderStatus(final String statusAction) {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.setMessage("Processing...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE_ORDER_STATUS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);
                            String responseMsg = rootJsonObject.getString(ApiConstants.KEY_RESPONSE);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                String toastMsg = "[" + getSupportActionBar().getTitle().toString() + "] ";
                                if (statusAction.equals(ApiConstants.PARAM_VALUE_ACCEPTED)) {
                                    toastMsg += "Order accepted successfully.";
                                } else if (statusAction.equals(ApiConstants.PARAM_VALUE_RE_ACCEPT)) {
                                    toastMsg += "Order re-accepted successfully.";
                                } else if (statusAction.equals(ApiConstants.PARAM_VALUE_REJECTED)) {
                                    toastMsg += "Order rejected successfully.";
                                }
                                Toast.makeText(AcceptRejectActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                                ActivityCompat.finishAffinity(AcceptRejectActivity.this);
                                startActivity(new Intent(AcceptRejectActivity.this, MainActivity.class));
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(AcceptRejectActivity.this, responseMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(AcceptRejectActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ORDER_ID, mOrderId);
                        params.put(ApiConstants.PARAM_USER_ID, mUserId);
                        params.put(ApiConstants.PARAM_BAKER_ID, mBakerId);
                        params.put(ApiConstants.PARAM_SUBTOTAL, mSubtotalStr);
                        params.put(ApiConstants.PARAM_DELIVERY_CHARGES, mDeliveryChargesStr);
                        params.put(ApiConstants.PARAM_TOTAL_AMOUNT, mTotalAmountStr);
                        params.put(ApiConstants.PARAM_REJECTED_REASON, mRejectedReason);
                        params.put(ApiConstants.PARAM_STATUS, statusAction);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(AcceptRejectActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }
}
