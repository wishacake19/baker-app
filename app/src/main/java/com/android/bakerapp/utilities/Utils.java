package com.android.bakerapp.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.bakerapp.R;
import com.android.bakerapp.activities.HelpActivity;
import com.android.bakerapp.helpers.ApiConstants;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    private static final String LOG_TAG = Utils.class.getSimpleName();

    private static String sFirebaseToken;
    private static final String SENDER_ID = "834136674544";

    public static boolean isNameValid(String name) {
        boolean check = false;
        String regexExp = "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$";
        if (name.matches(regexExp)) {
            check = true;
        }
        return check;
    }

    public static boolean isEmailValid(String email) {
        boolean check = false;
        String regexExp = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-zA-Z]{2,})$";
        if (email.matches(regexExp)) {
            check = true;
        }
        return check;
    }

    public static boolean isMobileNumberValid(String mobileNumber) {
        boolean check = false;
        if (mobileNumber.startsWith("+92") && mobileNumber.length() == 13) {
            check = true;
        }
        return check;
    }

    public static boolean isPinValid(String pin) {
        boolean check = false;
        String regexExp = "^[0-9+]{4}$";
        if (pin.matches(regexExp)) {
            check = true;
        }
        return check;
    }

    public static boolean isCnicValid(String cnic) {
        boolean check = false;
        String regexExp = "^[0-9+]{13}$";
        if (cnic.matches(regexExp)) {
            check = true;
        }
        return check;
    }


    public static void enableDisableUploadImageOptionInHelp(Context context, int bgDrawableId, boolean isEnabled) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            HelpActivity.sFrameLayoutUploadImage.setBackground(ContextCompat.getDrawable(context, bgDrawableId));
        }
        if (isEnabled) {
            HelpActivity.sButtonUploadAnImage.setEnabled(true);
        } else {
            HelpActivity.sButtonUploadAnImage.setEnabled(false);
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static String getFormattedCNIC(String cnic) {
        StringBuilder stringBuilder = new StringBuilder(cnic);
        stringBuilder.insert(5, "-");
        stringBuilder.insert(13, "-");
        return stringBuilder.toString();
    }

    public static boolean isStringEmptyOrNull(String str) {
        if (str.isEmpty() || str.equals("") || str.equalsIgnoreCase("null") || str == null) {
            return true;
        } else {
            return false;
        }
    }

    public static void showOkAlertDialog(Context context, String title, String msg, int iconResId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setCancelable(true);
        if (iconResId != 0) {
            builder.setIcon(iconResId);
        }
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showError(EditText editText, String errMessage, TextView errorTextView) {
        errorTextView.setVisibility(View.VISIBLE);
        errorTextView.setText(errMessage);
        editText.requestFocus();
    }

    public static void hideError(TextView errorTextView) {
        errorTextView.setVisibility(View.GONE);
        errorTextView.setText("");
    }

    public static String getBase64EncodedFromBitmap(Bitmap imageBitmap) {
        if (imageBitmap == null) {
            return "";
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    public static String getPathFromBitmap(Activity activity, Bitmap imageBitmap) {
        return MediaStore.Images.Media.insertImage(activity.getContentResolver(), imageBitmap, "title", null);
    }

    public static String getFirebaseToken() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    sFirebaseToken = FirebaseInstanceId.getInstance().getToken(SENDER_ID, "FCM");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
        return sFirebaseToken;
    }

    public static long convertStringDateToLong(String dateTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static void changeStatusBarBackground(Activity activity, int color) {
        try {
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color to any color with transparency
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(color);
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    public static void expand(final View view, int speedMultiplierValue) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) view.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(matchParentMeasureSpec, wrapContentMeasureSpec);

        final int targetHeight = view.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.getLayoutParams().height = 1;
        view.setVisibility(View.VISIBLE);

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration(((int) (targetHeight / view.getContext().getResources().getDisplayMetrics().density)) * speedMultiplierValue);
        view.startAnimation(animation);
    }

    public static void collapse(final View view, int speedMultiplierValue) {
        final int initialHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration(((int) (initialHeight / view.getContext().getResources().getDisplayMetrics().density)) * speedMultiplierValue);
        view.startAnimation(animation);
    }

    public static int getOrdersActivityThemeByStatus(String status) {
        int styleId = R.style.AppTheme;
        if (status.equals(ApiConstants.PARAM_VALUE_NEW)) {
            styleId = R.style.AppTheme_NoActionBar_NewOrdersTheme;
        } else if (status.equals(ApiConstants.PARAM_VALUE_ACCEPTED)) {
            styleId = R.style.AppTheme_NoActionBar_AcceptedOrdersTheme;
        } else if (status.equals(ApiConstants.PARAM_VALUE_REJECTED)) {
            styleId = R.style.AppTheme_NoActionBar_RejectedOrdersTheme;
        } else if (status.equals(ApiConstants.PARAM_VALUE_CONFIRMED)) {
            styleId = R.style.AppTheme_NoActionBar_ConfirmedOrdersTheme;
        } else if (status.equals(ApiConstants.PARAM_VALUE_CANCELLED)) {
            styleId = R.style.AppTheme_NoActionBar_CancelledOrdersTheme;
        } else if (status.equals(ApiConstants.PARAM_VALUE_ONGOING)) {
            styleId = R.style.AppTheme_NoActionBar_OngoingOrdersTheme;
        } else if (status.equals(ApiConstants.PARAM_VALUE_COMPLETED)) {
            styleId = R.style.AppTheme_NoActionBar_CompletedAppTheme;
        }
        return styleId;
    }

    public static int getOrdersActivityPrimaryColorByStatus(String status) {
        int colorId = R.color.colorOrderStatusPending;
        if (status.equals(ApiConstants.PARAM_VALUE_NEW)) {
            colorId = R.color.colorOrderStatusPending;
        } else if (status.equals(ApiConstants.PARAM_VALUE_ACCEPTED)) {
            colorId = R.color.colorOrderStatusAccepted;
        } else if (status.equals(ApiConstants.PARAM_VALUE_REJECTED)) {
            colorId = R.color.colorOrderStatusRejected;
        } else if (status.equals(ApiConstants.PARAM_VALUE_CONFIRMED)) {
            colorId = R.color.colorOrderStatusConfirmed;
        } else if (status.equals(ApiConstants.PARAM_VALUE_CANCELLED)) {
            colorId = R.color.colorOrderStatusCancelled;
        } else if (status.equals(ApiConstants.PARAM_VALUE_ONGOING)) {
            colorId = R.color.colorOrderStatusOngoing;
        } else if (status.equals(ApiConstants.PARAM_VALUE_COMPLETED)) {
            colorId = R.color.colorOrderStatusCompleted;
        }
        return colorId;
    }

    public static String getOrderStatusValueByOrderStatusCode(String status) {
        String orderStatus = ApiConstants.PARAM_VALUE_NEW;
        if (status.equals("1")) {
            orderStatus = ApiConstants.PARAM_VALUE_NEW;
        } else if (status.equals("2")) {
            orderStatus = ApiConstants.PARAM_VALUE_ACCEPTED;
        } else if (status.equals("3")) {
            orderStatus = ApiConstants.PARAM_VALUE_REJECTED;
        } else if (status.equals("4")) {
            orderStatus = ApiConstants.PARAM_VALUE_CONFIRMED;
        } else if (status.equals("5")) {
            orderStatus = ApiConstants.PARAM_VALUE_CANCELLED;
        } else if (status.equals("6")) {
            orderStatus = ApiConstants.PARAM_VALUE_ONGOING;
        } else if (status.equals("7")) {
            orderStatus = ApiConstants.PARAM_VALUE_COMPLETED;
        }
        return orderStatus;
    }

    public static void setUpOrderStatus(Context context, TextView textView, String text, int colorResId, int drawableResId) {
        textView.setText(text);
        textView.setTextColor(ContextCompat.getColor(context, colorResId));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            textView.setBackground(ContextCompat.getDrawable(context, drawableResId));
        }
    }
}
