package com.android.bakerapp.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.activities.FullscreenImageActivity;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.PermissionUtils;
import com.android.bakerapp.utilities.UriRealPathUtils;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class Top3CakesFragment extends Fragment implements View.OnClickListener {

    private ImageView mImageView1stCake, mImageView1stAdd, mImageView1stEdit, mImageView1stDelete,
            mImageView2ndCake, mImageView2ndAdd, mImageView2ndEdit, mImageView2ndDelete,
            mImageView3rdCake, mImageView3rdAdd, mImageView3rdEdit, mImageView3rdDelete;

    private String mId, mSliderImage1, mSliderImage2, mSliderImage3, mSliderImageBase64 = "", mWhichSliderImage;

    private ProgressDialog mProgressDialog;

    private static String sAppName;

    // For SDK < 24
    private Uri mPhotoUri;

    private static final int REQUEST_CODE_GALLERY = 01;
    private static final int REQUEST_CODE_CAMERA = 02;

    private static final String LOG_TAG = Top3CakesFragment.class.getSimpleName();

    public Top3CakesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top3_cakes, container, false);

        initViews(view);

        sAppName = getString(R.string.app_name);

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setCancelable(false);

        loadBakerSliderImagesFromSharedPref();

        setUpClickListeners();

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == mImageView1stCake) {
            if (!Utils.isStringEmptyOrNull(mSliderImage1)) {
                Intent intent = new Intent(getContext(), FullscreenImageActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_FULLSCREEN_IMAGE, mSliderImage1);
                startActivity(intent);
            }
        }
        if (v == mImageView2ndCake) {
            if (!Utils.isStringEmptyOrNull(mSliderImage2)) {
                Intent intent = new Intent(getContext(), FullscreenImageActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_FULLSCREEN_IMAGE, mSliderImage2);
                startActivity(intent);
            }
        }
        if (v == mImageView3rdCake) {
            if (!Utils.isStringEmptyOrNull(mSliderImage3)) {
                Intent intent = new Intent(getContext(), FullscreenImageActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_FULLSCREEN_IMAGE, mSliderImage3);
                startActivity(intent);
            }
        }
        if (v == mImageView1stAdd || v == mImageView1stEdit) {
            mWhichSliderImage = ApiConstants.PARAM_SLIDER_IMAGE1;
            showTakeUploadPhotoDialog();
        }
        if (v == mImageView2ndAdd || v == mImageView2ndEdit) {
            mWhichSliderImage = ApiConstants.PARAM_SLIDER_IMAGE2;
            showTakeUploadPhotoDialog();
        }
        if (v == mImageView3rdAdd || v == mImageView3rdEdit) {
            mWhichSliderImage = ApiConstants.PARAM_SLIDER_IMAGE3;
            showTakeUploadPhotoDialog();
        }
        if (v == mImageView1stDelete) {
            mSliderImageBase64 = ApiConstants.PARAM_VALUE_REMOVE;
            onClickAddEditDelete(ApiConstants.PARAM_SLIDER_IMAGE1);
        }
        if (v == mImageView2ndDelete) {
            mSliderImageBase64 = ApiConstants.PARAM_VALUE_REMOVE;
            onClickAddEditDelete(ApiConstants.PARAM_SLIDER_IMAGE2);
        }
        if (v == mImageView3rdDelete) {
            mSliderImageBase64 = ApiConstants.PARAM_VALUE_REMOVE;
            onClickAddEditDelete(ApiConstants.PARAM_SLIDER_IMAGE3);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean writeStoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeStoragePermission) {
                    openGallery();
                }
            }
        }
        if (requestCode == PermissionUtils.REQUEST_CODE_CAMERA_OR_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (cameraPermission) {
                    if (!PermissionUtils.checkGalleryPermission(getContext())) {
                        boolean writeStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        if (writeStoragePermission) {
                            openCamera();
                        }
                    }
                    openCamera();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                try {
                    Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    mSliderImageBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                    onClickAddEditDelete(mWhichSliderImage);
                } catch (IOException e) {
                    e.getMessage();
                }
            }
        }
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
                        mSliderImageBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                        onClickAddEditDelete(mWhichSliderImage);
                    } else {
                        Toast.makeText(getContext(), "An unknown error has occurred while saving the image. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                String imagePath = UriRealPathUtils.getUriRealPath(getContext(), mPhotoUri);
                Bitmap imageBitmap = null;
                if (!imagePath.isEmpty()) {
                    imageBitmap = BitmapFactory.decodeFile(imagePath);
                }
                mSliderImageBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                onClickAddEditDelete(mWhichSliderImage);
            }
        }
    }

    private void initViews(View view) {
        mImageView1stCake = (ImageView) view.findViewById(R.id.img_1st_cake);
        mImageView1stAdd = (ImageView) view.findViewById(R.id.img_1st_add);
        mImageView1stEdit = (ImageView) view.findViewById(R.id.img_1st_edit);
        mImageView1stDelete = (ImageView) view.findViewById(R.id.img_1st_delete);
        mImageView2ndCake = (ImageView) view.findViewById(R.id.img_2nd_cake);
        mImageView2ndAdd = (ImageView) view.findViewById(R.id.img_2nd_add);
        mImageView2ndEdit = (ImageView) view.findViewById(R.id.img_2nd_edit);
        mImageView2ndDelete = (ImageView) view.findViewById(R.id.img_2nd_delete);
        mImageView3rdCake = (ImageView) view.findViewById(R.id.img_3rd_cake);
        mImageView3rdAdd = (ImageView) view.findViewById(R.id.img_3rd_add);
        mImageView3rdEdit = (ImageView) view.findViewById(R.id.img_3rd_edit);
        mImageView3rdDelete = (ImageView) view.findViewById(R.id.img_3rd_delete);
    }

    private void loadBakerSliderImagesFromSharedPref() {
        mId = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID);
        mSliderImage1 = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE1);
        mSliderImage2 = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE2);
        mSliderImage3 = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE3);

        loadImage(mSliderImage1, mImageView1stCake, mImageView1stAdd, mImageView1stEdit, mImageView1stDelete);
        loadImage(mSliderImage2, mImageView2ndCake, mImageView2ndAdd, mImageView2ndEdit, mImageView2ndDelete);
        loadImage(mSliderImage3, mImageView3rdCake, mImageView3rdAdd, mImageView3rdEdit, mImageView3rdDelete);
    }

    private void loadImage(String image, ImageView imageViewCake, ImageView imageViewAdd, ImageView imageViewEdit, ImageView imageViewDelete) {
        if (!Utils.isStringEmptyOrNull(image)) {
            Picasso.get().load(image).fit().placeholder(R.drawable.ic_placeholder_bg).into(imageViewCake);
            imageViewAdd.setVisibility(View.GONE);
            imageViewEdit.setVisibility(View.VISIBLE);
            imageViewDelete.setVisibility(View.VISIBLE);
        } else {
            Picasso.get().load(R.drawable.ic_placeholder_bg).placeholder(R.drawable.ic_placeholder_bg).into(imageViewCake);
            imageViewAdd.setVisibility(View.VISIBLE);
            imageViewEdit.setVisibility(View.GONE);
            imageViewDelete.setVisibility(View.GONE);
        }
    }

    private void setUpClickListeners() {
        mImageView1stCake.setOnClickListener(this);
        mImageView1stAdd.setOnClickListener(this);
        mImageView1stEdit.setOnClickListener(this);
        mImageView1stDelete.setOnClickListener(this);
        mImageView2ndCake.setOnClickListener(this);
        mImageView2ndAdd.setOnClickListener(this);
        mImageView2ndEdit.setOnClickListener(this);
        mImageView2ndDelete.setOnClickListener(this);
        mImageView3rdCake.setOnClickListener(this);
        mImageView3rdAdd.setOnClickListener(this);
        mImageView3rdEdit.setOnClickListener(this);
        mImageView3rdDelete.setOnClickListener(this);
    }

    private void showTakeUploadPhotoDialog() {
        final CharSequence[] items = new CharSequence[]{"Take photo", "Upload photo"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take photo")) {
                    if (PermissionUtils.checkCameraPermission(getContext())) {
                        openCamera();
                    }
                } else if (items[item].equals("Upload photo")) {
                    if (PermissionUtils.checkCameraPermission(getContext())) {
                        openGallery();
                    }
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture"), REQUEST_CODE_GALLERY);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFileForSDK24AndAbove();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(LOG_TAG, "IOException || " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        getContext().getPackageName() + ".fileprovider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
            }
        } else {
            mPhotoUri = Uri.fromFile(createImageFileForSDKBelow24());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
            startActivityForResult(intent, REQUEST_CODE_CAMERA);
        }
    }

    private File createImageFileForSDK24AndAbove() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + timeStamp;
        File storageDirectory = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(fileName, ".jpg", storageDirectory);
    }

    private File createImageFileForSDKBelow24() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), sAppName);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    private void onClickAddEditDelete(final String paramAttribute) {
        if (!Utils.isNetworkAvailable(getContext())) {
            Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            return;
        }
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                updateSliderImage(paramAttribute);
                return null;
            }
        }.execute();
    }

    private void updateSliderImage(final String paramAttribute) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE_BAKER_SLIDER_IMAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        mId = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                        String image = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_IMAGE);
                        String firstName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                        String lastName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                        String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                        String mobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                        String cnic = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_CNIC);
                        String pin = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PIN);
                        String locationId = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_ID);
                        String locationName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_NAME);
                        String locationAddress = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                        String locationLatitude = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_LATITUDE);
                        String locationLongitude = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_LONGITUDE);
                        mSliderImage1 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE1);
                        mSliderImage2 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE2);
                        mSliderImage3 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE3);
                        String rating = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_RATING))
                                ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_RATING);
                        String activeStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACTIVE_STATUS);
                        String bakerEarning = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_BAKER_EARNING);

                        if (activeStatus.equals("1")) {
                            activeStatus = ApiConstants.PARAM_VALUE_ONLINE;
                        } else if (activeStatus.equals("0")) {
                            activeStatus = ApiConstants.PARAM_VALUE_OFFLINE;
                        }

                        SharedPrefSingleton.getInstance(getContext()).saveBakerData(mId, image, firstName, lastName, email,
                                mobileNumber, cnic, pin, locationId, locationName, locationAddress, locationLatitude,
                                locationLongitude, mSliderImage1, mSliderImage2, mSliderImage3, rating, bakerEarning, activeStatus);

                        if (mSliderImageBase64.equals(ApiConstants.PARAM_VALUE_REMOVE)) {
                            if (paramAttribute.equals(ApiConstants.PARAM_SLIDER_IMAGE1)) {
                                mSliderImage1 = "";
                                Picasso.get().load(R.drawable.ic_placeholder_bg).placeholder(R.drawable.ic_placeholder_bg).into(mImageView1stCake);
                                mImageView1stAdd.setVisibility(View.VISIBLE);
                                mImageView1stEdit.setVisibility(View.GONE);
                                mImageView1stDelete.setVisibility(View.GONE);
                            } else if (paramAttribute.equals(ApiConstants.PARAM_SLIDER_IMAGE2)) {
                                mSliderImage2 = "";
                                Picasso.get().load(R.drawable.ic_placeholder_bg).placeholder(R.drawable.ic_placeholder_bg).into(mImageView2ndCake);
                                mImageView2ndAdd.setVisibility(View.VISIBLE);
                                mImageView2ndEdit.setVisibility(View.GONE);
                                mImageView2ndDelete.setVisibility(View.GONE);
                            } else if (paramAttribute.equals(ApiConstants.PARAM_SLIDER_IMAGE3)) {
                                mSliderImage3 = "";
                                Picasso.get().load(R.drawable.ic_placeholder_bg).placeholder(R.drawable.ic_placeholder_bg).into(mImageView3rdCake);
                                mImageView3rdAdd.setVisibility(View.VISIBLE);
                                mImageView3rdEdit.setVisibility(View.GONE);
                                mImageView3rdDelete.setVisibility(View.GONE);
                            }
                        } else {
                            if (paramAttribute.equals(ApiConstants.PARAM_SLIDER_IMAGE1)) {
                                Picasso.get().load(mSliderImage1).placeholder(R.drawable.ic_placeholder_bg).into(mImageView1stCake);
                                mImageView1stAdd.setVisibility(View.GONE);
                                mImageView1stEdit.setVisibility(View.VISIBLE);
                                mImageView1stDelete.setVisibility(View.VISIBLE);
                            } else if (paramAttribute.equals(ApiConstants.PARAM_SLIDER_IMAGE2)) {
                                Picasso.get().load(mSliderImage2).placeholder(R.drawable.ic_placeholder_bg).into(mImageView2ndCake);
                                mImageView2ndAdd.setVisibility(View.GONE);
                                mImageView2ndEdit.setVisibility(View.VISIBLE);
                                mImageView2ndDelete.setVisibility(View.VISIBLE);
                            } else if (paramAttribute.equals(ApiConstants.PARAM_SLIDER_IMAGE3)) {
                                Picasso.get().load(mSliderImage3).placeholder(R.drawable.ic_placeholder_bg).into(mImageView3rdCake);
                                mImageView3rdAdd.setVisibility(View.GONE);
                                mImageView3rdEdit.setVisibility(View.VISIBLE);
                                mImageView3rdDelete.setVisibility(View.VISIBLE);
                            }
                        }

                        Toast.makeText(getContext(), "Profile updated successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                            Toast.makeText(getContext(), rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(getContext(), toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_ID, SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                params.put(ApiConstants.PARAM_SLIDER_IMAGE_ATTR, paramAttribute);
                // Update with new profile photo
                if (!Utils.isStringEmptyOrNull(mSliderImageBase64)) {
                    params.put(ApiConstants.PARAM_SLIDER_IMAGE, mSliderImageBase64);
                }
                // Remove profile photo
                else if (mSliderImageBase64.equals(ApiConstants.PARAM_VALUE_REMOVE)) {
                    params.put(ApiConstants.PARAM_SLIDER_IMAGE, ApiConstants.PARAM_VALUE_REMOVE);
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }
}
