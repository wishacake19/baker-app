package com.android.bakerapp.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bakerapp.R;
import com.android.bakerapp.activities.FullscreenImageActivity;
import com.android.bakerapp.helpers.ApiConstants;
import com.android.bakerapp.helpers.Constants;
import com.android.bakerapp.helpers.ErrorTextWatcher;
import com.android.bakerapp.helpers.SharedPrefSingleton;
import com.android.bakerapp.helpers.VolleyRequestHandlerSingleton;
import com.android.bakerapp.utilities.PermissionUtils;
import com.android.bakerapp.utilities.UriRealPathUtils;
import com.android.bakerapp.utilities.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class AboutFragment extends Fragment implements View.OnClickListener {

    private EditText mEditTextFirstName, mEditTextLastName, mEditTextEmail,
            mEditTextMobileNumber, mEditTextCnic, mEditTextAddress;
    private CircleImageView mCircleImageViewProfilePhoto;
    private ImageView mImageViewAdd, mImageViewEdit;
    private TextView mTextViewHelperFirstName, mTextViewHelperLastName, mTextViewHelperEmail, mTextViewHelperMobileNumber,
            mTextViewHelperAddress, mTextViewVerify;
    private Button mButtonUpdate;

    private String mId, mImage, mFirstName, mLastName, mEmail, mMobileNumber, mCnic, mLocationId, mLocationName,
            mLocationAddress, mLocationLatitude, mLocationLongitude, mProfilePhotoBase64 = "", mViewPhotoUri;
    private boolean mIsMobileNumberVerified = true;

    private static CharSequence[] sImageItems;

    private ProgressDialog mProgressDialog;

    private static String sAppName;

    // For SDK < 24
    private Uri mPhotoUri;

    private static final int REQUEST_CODE_GALLERY = 01;
    private static final int REQUEST_CODE_CAMERA = 02;
    private static final int VERIFY_MOBILE_NUMBER_REQUEST_CODE = 21;
    private static final int PLACE_PICKER_REQUEST_CODE = 14;

    private static final String LOG_TAG = AboutFragment.class.getSimpleName();

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        initViews(view);

        sAppName = getString(R.string.app_name);

        mEditTextFirstName.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperFirstName));
        mEditTextLastName.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperLastName));
        mEditTextEmail.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperEmail));
        mEditTextMobileNumber.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperMobileNumber));
        mEditTextAddress.addTextChangedListener(new ErrorTextWatcher(mTextViewHelperAddress));

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setCancelable(false);

        mEditTextMobileNumber.setText(Constants.MOBILE_NUMBER_PAK_PREFIX);
        Selection.setSelection(mEditTextMobileNumber.getText(), mEditTextMobileNumber.getText().length());

        mEditTextMobileNumber.addTextChangedListener(new TextWatcher() {
            String text;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                text = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().startsWith(Constants.MOBILE_NUMBER_PAK_PREFIX)) {
                    mEditTextMobileNumber.setText(text);
                    Selection.setSelection(mEditTextMobileNumber.getText(), mEditTextMobileNumber.getText().length());
                }
                String mobileNumberSharedPref = SharedPrefSingleton.getInstance(getContext()).getBakerData()
                        .get(Constants.SHARED_PREF_KEY_BAKER_MOBILE_NUMBER);
                if (mEditTextMobileNumber.getText().toString().equals(mobileNumberSharedPref)) {
                    mIsMobileNumberVerified = true;
                    mTextViewVerify.setVisibility(View.GONE);
                } else {
                    mIsMobileNumberVerified = false;
                    mTextViewVerify.setVisibility(View.VISIBLE);
                }
            }
        });

        loadBakerInfoFromSharedPref();
        populateDataFromSharedPref();
        mTextViewVerify.setVisibility(View.GONE);

        mCircleImageViewProfilePhoto.setOnClickListener(this);
        mTextViewVerify.setOnClickListener(this);
        mButtonUpdate.setOnClickListener(this);
        mEditTextAddress.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == mCircleImageViewProfilePhoto) {
            onClickProfilePhoto();
        }
        if (view == mTextViewVerify) {
            onClickVerify();
        }
        if (view == mButtonUpdate) {
            onClickUpdate("Please wait...");
        }
        if (view == mEditTextAddress) {
            if (PermissionUtils.checkLocationPermission(getContext())) {
                openPlacePickerWidget();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_LOCATION) {
            if (grantResults.length > 0) {
                boolean locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (locationPermission) {
                    openPlacePickerWidget();
                }
            }
        }
        if (requestCode == PermissionUtils.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean writeStoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeStoragePermission) {
                    openGallery();
                }
            }
        }
        if (requestCode == PermissionUtils.REQUEST_CODE_CAMERA_OR_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (cameraPermission) {
                    if (!PermissionUtils.checkGalleryPermission(getContext())) {
                        boolean writeStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        if (writeStoragePermission) {
                            openCamera();
                        }
                    }
                    openCamera();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && requestCode == PLACE_PICKER_REQUEST_CODE) {
            Place place = PlacePicker.getPlace(getContext(), data);
            mLocationId = place.getId();
            mLocationName = place.getName().toString();
            mLocationAddress = place.getAddress().toString();
            mLocationLatitude = String.valueOf(place.getLatLng().latitude);
            mLocationLongitude = String.valueOf(place.getLatLng().longitude);
            mEditTextAddress.setText(place.getName().toString() + "\n" + place.getAddress().toString());
        }
        if (requestCode == VERIFY_MOBILE_NUMBER_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            AccountKitLoginResult result = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if (result.getError() != null) {
                return;
            } else if (result.wasCancelled()) {
                return;
            } else {
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        mIsMobileNumberVerified = true;
                        mMobileNumber = String.valueOf(account.getPhoneNumber());
                        mTextViewVerify.setVisibility(View.GONE);
                        mEditTextMobileNumber.setText(mMobileNumber);
                        Utils.hideError(mTextViewHelperMobileNumber);
                        mEditTextMobileNumber.setEnabled(false);
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {
                    }
                });
            }
        }
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
//                mCircleImageViewProfilePhoto.setImageURI(uri);
                mViewPhotoUri = uri.toString();
                try {
                    Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    mProfilePhotoBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                    mCircleImageViewProfilePhoto.setImageBitmap(imageBitmap);
                    sImageItems = new CharSequence[]{"View photo", "Take photo", "Upload photo"};
                    mImageViewAdd.setVisibility(View.GONE);
                    mImageViewEdit.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.getMessage();
                }
                Log.d(LOG_TAG, mProfilePhotoBase64);
            }
        }
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
                        Uri uri = Uri.parse(Utils.getPathFromBitmap(getActivity(), imageBitmap));
                        mViewPhotoUri = uri.toString();
                        mCircleImageViewProfilePhoto.setImageBitmap(imageBitmap);
                        sImageItems = new CharSequence[]{"View photo", "Take photo", "Upload photo"};
                        mImageViewAdd.setVisibility(View.GONE);
                        mImageViewEdit.setVisibility(View.VISIBLE);
                        mProfilePhotoBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
                    } else {
                        Toast.makeText(getContext(), "An unknown error has occurred while saving the image. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                mCircleImageViewProfilePhoto.setImageURI(mPhotoUri);
                mViewPhotoUri = mPhotoUri.toString();
                sImageItems = new CharSequence[]{"View photo", "Take photo", "Upload photo"};
                mImageViewAdd.setVisibility(View.GONE);
                mImageViewEdit.setVisibility(View.VISIBLE);
                String imagePath = UriRealPathUtils.getUriRealPath(getContext(), mPhotoUri);
                Bitmap imageBitmap = null;
                if (!imagePath.isEmpty()) {
                    imageBitmap = BitmapFactory.decodeFile(imagePath);
                }
                mProfilePhotoBase64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
            }
        }
    }

    private void initViews(View view) {
        mCircleImageViewProfilePhoto = (CircleImageView) view.findViewById(R.id.civ_profile_photo);
        mImageViewAdd = (ImageView) view.findViewById(R.id.img_add);
        mImageViewEdit = (ImageView) view.findViewById(R.id.img_edit);
        mEditTextFirstName = (EditText) view.findViewById(R.id.et_first_name);
        mEditTextLastName = (EditText) view.findViewById(R.id.et_last_name);
        mEditTextEmail = (EditText) view.findViewById(R.id.et_email);
        mEditTextCnic = (EditText) view.findViewById(R.id.et_cnic);
        mEditTextMobileNumber = (EditText) view.findViewById(R.id.et_mobile_number);
        mEditTextAddress = (EditText) view.findViewById(R.id.et_address);
        mTextViewHelperFirstName = (TextView) view.findViewById(R.id.tv_helper_first_name);
        mTextViewHelperLastName = (TextView) view.findViewById(R.id.tv_helper_last_name);
        mTextViewHelperEmail = (TextView) view.findViewById(R.id.tv_helper_email);
        mTextViewHelperMobileNumber = (TextView) view.findViewById(R.id.tv_helper_mobile_number);
        mTextViewHelperAddress = (TextView) view.findViewById(R.id.tv_helper_address);
        mButtonUpdate = (Button) view.findViewById(R.id.btn_update);
        mTextViewVerify = (TextView) view.findViewById(R.id.tv_verify);
    }

    private boolean isValid() {
        boolean flag = false;

        mFirstName = mEditTextFirstName.getText().toString().trim();
        mLastName = mEditTextLastName.getText().toString().trim();
        mEmail = mEditTextEmail.getText().toString().trim();
        mMobileNumber = mEditTextMobileNumber.getText().toString().trim();

        if (mFirstName.isEmpty()) {
            Utils.showError(mEditTextFirstName, "First name is required.", mTextViewHelperFirstName);
        } else if (Utils.isNameValid(mFirstName) == false) {
            Utils.showError(mEditTextFirstName, "Please enter a valid first name.", mTextViewHelperFirstName);
        } else if (mLastName.isEmpty()) {
            Utils.showError(mEditTextLastName, "Last name is required.", mTextViewHelperLastName);
        } else if (Utils.isNameValid(mLastName) == false) {
            Utils.showError(mEditTextLastName, "Please enter a valid last name.", mTextViewHelperLastName);
        } else if (mEmail.isEmpty()) {
            Utils.showError(mEditTextEmail, "Email address is required.", mTextViewHelperEmail);
        } else if (Utils.isEmailValid(mEmail) == false) {
            Utils.showError(mEditTextEmail, "Please enter a valid email address.", mTextViewHelperEmail);
        } else if (mMobileNumber.isEmpty()) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is required.", mTextViewHelperMobileNumber);
        } else if (Utils.isMobileNumberValid(mMobileNumber) == false) {
            Utils.showError(mEditTextMobileNumber, "Please enter a valid mobile number.", mTextViewHelperMobileNumber);
        } else if (!mIsMobileNumberVerified) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is not verified.", mTextViewHelperMobileNumber);
        } else if (mEditTextAddress.getText().toString().trim().isEmpty()) {
            Utils.showError(mEditTextAddress, "Address is required.", mTextViewHelperAddress);
        } else {
            flag = true;
        }
        return flag;
    }

    private void loadBakerInfoFromSharedPref() {
        mId = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID);
        mImage = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_IMAGE);
        mFirstName = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_FIRST_NAME);
        mLastName = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LAST_NAME);
        mEmail = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_EMAIL);
        mMobileNumber = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_MOBILE_NUMBER);
        mCnic = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_CNIC);
        mLocationId = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ID);
        mLocationName = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_NAME);
        mLocationAddress = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ADDRESS);
        mLocationLatitude = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LATITUDE);
        mLocationLongitude = SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LONGITUDE);
    }

    private void populateDataFromSharedPref() {
        if (!Utils.isStringEmptyOrNull(mImage)) {
            mImageViewAdd.setVisibility(View.GONE);
            mImageViewEdit.setVisibility(View.VISIBLE);
            Picasso.get().load(mImage).fit().placeholder(R.drawable.ic_default_profile_pic).into(mCircleImageViewProfilePhoto);
            mViewPhotoUri = mImage;
            sImageItems = new CharSequence[]{"View photo", "Take photo", "Upload photo", "Remove photo"};
        } else {
            mImageViewAdd.setVisibility(View.VISIBLE);
            mImageViewEdit.setVisibility(View.GONE);
            Picasso.get().load(R.drawable.ic_default_profile_pic).placeholder(R.drawable.ic_default_profile_pic).into(mCircleImageViewProfilePhoto);
            sImageItems = new CharSequence[]{"Take photo", "Upload photo"};
        }

        mEditTextFirstName.setText(mFirstName);
        mEditTextLastName.setText(mLastName);
        mEditTextEmail.setText(mEmail);
        mEditTextMobileNumber.setText(mMobileNumber);
        mEditTextCnic.setText(mCnic);
        mEditTextAddress.setText(mLocationName + "\n" + mLocationAddress);
    }

    private void openPlacePickerWidget() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(getActivity());
            startActivityForResult(intent, PLACE_PICKER_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void onClickVerify() {
        mMobileNumber = mEditTextMobileNumber.getText().toString().trim();
        if (mMobileNumber.isEmpty()) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is required.", mTextViewHelperMobileNumber);
        } else if (Utils.isMobileNumberValid(mMobileNumber) == false) {
            Utils.showError(mEditTextMobileNumber, "Mobile number is invalid.", mTextViewHelperMobileNumber);
        } else {
            if (!Utils.isNetworkAvailable(getContext())) {
                Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                return;
            }
            verify();
        }
    }

    private void verify() {
        Intent intent = new Intent(getContext(), AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder accountKitConfigurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);  // Use token when Yes 'Enable Client Access Token Flow'
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, accountKitConfigurationBuilder.build());
        startActivityForResult(intent, VERIFY_MOBILE_NUMBER_REQUEST_CODE);
    }

    private void onClickProfilePhoto() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setItems(sImageItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (sImageItems[item].equals("View photo")) {
                    Intent intent = new Intent(getContext(), FullscreenImageActivity.class);
                    intent.putExtra(Constants.EXTRA_KEY_FULLSCREEN_IMAGE, mViewPhotoUri);
                    startActivity(intent);
                }
                if (sImageItems[item].equals("Take photo")) {
                    if (PermissionUtils.checkCameraPermission(getContext())) {
                        openCamera();
                    }
                } else if (sImageItems[item].equals("Upload photo")) {
                    if (PermissionUtils.checkCameraPermission(getContext())) {
                        openGallery();
                    }
                } else if (sImageItems[item].equals("Remove photo")) {
                    mProfilePhotoBase64 = ApiConstants.PARAM_VALUE_REMOVE;
                    onClickUpdate("Removing profile photo...");
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture"), REQUEST_CODE_GALLERY);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFileForSDK24AndAbove();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(LOG_TAG, "IOException || " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        getContext().getPackageName() + ".fileprovider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
            }
        } else {
            mPhotoUri = Uri.fromFile(createImageFileForSDKBelow24());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
            startActivityForResult(intent, REQUEST_CODE_CAMERA);
        }
    }

    private File createImageFileForSDK24AndAbove() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + timeStamp;
        File storageDirectory = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(fileName, ".jpg", storageDirectory);
    }

    private File createImageFileForSDKBelow24() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), sAppName);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    private void onClickUpdate(String progressDialogText) {
        if (isValid()) {
            if (!Utils.isNetworkAvailable(getContext())) {
                Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                return;
            }
            mProgressDialog.setMessage(progressDialogText);
            mProgressDialog.show();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    update();
                    return null;
                }
            }.execute();
        }
    }

    private void update() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        mId = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                        mImage = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_IMAGE);
                        mFirstName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                        mLastName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                        mEmail = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                        mMobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                        mCnic = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_CNIC);
                        String pin = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PIN);
                        mLocationId = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_ID);
                        mLocationName = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_NAME);
                        mLocationAddress = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                        mLocationLatitude = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_LATITUDE);
                        mLocationLongitude = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LOCATION_LONGITUDE);
                        String sliderImage1 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE1);
                        String sliderImage2 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE2);
                        String sliderImage3 = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_SLIDER_IMAGE3);
                        String rating = Utils.isStringEmptyOrNull(responseArray.getJSONObject(0).getString(ApiConstants.PARAM_RATING))
                                ? "0" : responseArray.getJSONObject(0).getString(ApiConstants.PARAM_RATING);
                        String activeStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACTIVE_STATUS);
                        String bakerEarning = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_BAKER_EARNING);

                        if (activeStatus.equals("1")) {
                            activeStatus = ApiConstants.PARAM_VALUE_ONLINE;
                        } else if (activeStatus.equals("0")) {
                            activeStatus = ApiConstants.PARAM_VALUE_OFFLINE;
                        }

                        SharedPrefSingleton.getInstance(getContext()).saveBakerData(mId, mImage, mFirstName, mLastName, mEmail,
                                mMobileNumber, mCnic, pin, mLocationId, mLocationName, mLocationAddress, mLocationLatitude,
                                mLocationLongitude, sliderImage1, sliderImage2, sliderImage3, rating, bakerEarning, activeStatus);

                        Toast.makeText(getContext(), "Profile updated successfully.", Toast.LENGTH_SHORT).show();
                        if (mProfilePhotoBase64.equals(ApiConstants.PARAM_VALUE_REMOVE)) {
                            mImageViewAdd.setVisibility(View.VISIBLE);
                            mImageViewEdit.setVisibility(View.GONE);
                            sImageItems = new CharSequence[]{"Take photo", "Upload photo"};
                            Picasso.get().load(R.drawable.ic_default_profile_pic).placeholder(R.drawable.ic_default_profile_pic).into(mCircleImageViewProfilePhoto);
                        } else {
                            getActivity().finish();
                        }
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("-2")) {
                            Utils.showError(mEditTextEmail, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), mTextViewHelperEmail);
                        } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("-3")) {
                            mEditTextMobileNumber.setEnabled(true);
                            mEditTextMobileNumber.requestFocus();
                            Selection.setSelection(mEditTextMobileNumber.getText(), mEditTextMobileNumber.getText().length());
                            mTextViewVerify.setVisibility(View.VISIBLE);
                            mIsMobileNumberVerified = false;
                            Utils.showError(mEditTextMobileNumber, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), mTextViewHelperMobileNumber);
                        } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                            Toast.makeText(getContext(), rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(getContext(), toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_ID, SharedPrefSingleton.getInstance(getContext()).getBakerData().get(Constants.SHARED_PREF_KEY_BAKER_ID));
                // Update with default profile photo
                if (mProfilePhotoBase64.equals("") && Utils.isStringEmptyOrNull(mImage)) {
                    params.put(ApiConstants.PARAM_IMAGE, "null");
                }
                // Update with existing profile photo
                else if (!Utils.isStringEmptyOrNull(mImage) && Utils.isStringEmptyOrNull(mProfilePhotoBase64)) {
                    mImage = mImage.substring(mImage.lastIndexOf("/") + 1);
                    params.put(ApiConstants.PARAM_IMAGE, mImage);
                }
                // Update with new profile photo
                else if (!Utils.isStringEmptyOrNull(mProfilePhotoBase64)) {
                    params.put(ApiConstants.PARAM_IMAGE, mProfilePhotoBase64);
                }
                // Remove profile photo
                else if (mProfilePhotoBase64.equals(ApiConstants.PARAM_VALUE_REMOVE)) {
                    params.put(ApiConstants.PARAM_IMAGE, ApiConstants.PARAM_VALUE_REMOVE);
                }
                params.put(ApiConstants.PARAM_FIRST_NAME, mFirstName);
                params.put(ApiConstants.PARAM_LAST_NAME, mLastName);
                params.put(ApiConstants.PARAM_EMAIL, mEmail.toLowerCase());
                params.put(ApiConstants.PARAM_MOBILE_NUMBER, mMobileNumber);
                params.put(ApiConstants.PARAM_LOCATION_ID, mLocationId);
                params.put(ApiConstants.PARAM_LOCATION_NAME, mLocationName);
                params.put(ApiConstants.PARAM_LOCATION_ADDRESS, mLocationAddress);
                params.put(ApiConstants.PARAM_LOCATION_LATITUDE, mLocationLatitude);
                params.put(ApiConstants.PARAM_LOCATION_LONGITUDE, mLocationLongitude);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }
}
