package com.android.bakerapp.helpers;

public class Constants {

    // Shared pref constants
    public static final String SHARED_PREF_NAME_BAKER = "shared-pref-user";
    public static final String SHARED_PREF_KEY_BAKER_ID = "baker-id";
    public static final String SHARED_PREF_KEY_BAKER_IMAGE = "baker-image";
    public static final String SHARED_PREF_KEY_BAKER_FIRST_NAME = "baker-first-name";
    public static final String SHARED_PREF_KEY_BAKER_LAST_NAME = "baker-last-name";
    public static final String SHARED_PREF_KEY_BAKER_EMAIL = "baker-email";
    public static final String SHARED_PREF_KEY_BAKER_MOBILE_NUMBER = "baker-mobile-number";
    public static final String SHARED_PREF_KEY_BAKER_CNIC = "baker-cnic";
    public static final String SHARED_PREF_KEY_BAKER_PIN = "baker-pin";
    public static final String SHARED_PREF_KEY_BAKER_LOCATION_ID = "baker-location-id";
    public static final String SHARED_PREF_KEY_BAKER_LOCATION_NAME = "baker-location-name";
    public static final String SHARED_PREF_KEY_BAKER_LOCATION_ADDRESS = "baker-location-address";
    public static final String SHARED_PREF_KEY_BAKER_LOCATION_LATITUDE = "baker-location-latitude";
    public static final String SHARED_PREF_KEY_BAKER_LOCATION_LONGITUDE = "baker-location-longitude";
    public static final String SHARED_PREF_KEY_BAKER_SLIDER_IMAGE1 = "baker-slider-image1";
    public static final String SHARED_PREF_KEY_BAKER_SLIDER_IMAGE2 = "baker-slider-image2";
    public static final String SHARED_PREF_KEY_BAKER_SLIDER_IMAGE3 = "baker-slider-image3";
    public static final String SHARED_PREF_KEY_BAKER_RATING = "baker-rating";
    public static final String SHARED_PREF_KEY_BAKER_EARNING = "baker-earning";
    public static final String SHARED_PREF_KEY_BAKER_ACTIVE_STATUS = "baker-active-status";
    public static final String SHARED_PREF_KEY_NOTIFICATION_UNIQUE_REQUEST_CODE = "notification-unique-request-code";
    public static final String SHARED_PREF_KEY_ORDER_ID = "order-id";
    public static final String SHARED_PREF_KEY_IS_ORDER_TO_BE_LOADED_FROM_SERVER = "is-order-to-be-loaded-from-server";

    // Intent Extras
    public static final String EXTRA_KEY_IS_PHOTOS_OPENED_AFTER_SPLASH = "is-photos-opened-after-splash";
    public static final String EXTRA_KEY_FULLSCREEN_IMAGE = "key-fullscreen-image";
    public static final String EXTRA_KEY_ORDER_IMAGES = "extra-key-order-images";
    public static final String EXTRA_KEY_ORDER_STATUS = "extra-key-order-status";
    public static final String EXTRA_KEY_ORDER_ID = "extra-key-order-id";
    public static final String EXTRA_KEY_ORDER_BAKER_ID = "extra-key-order-baker-id";
    public static final String EXTRA_KEY_ORDER_QUANTITY = "extra-key-order-quantity";
    public static final String EXTRA_KEY_ORDER_POUNDS = "extra-key-order-pounds";
    public static final String EXTRA_KEY_ORDER_DESCRIPTION = "extra-key-order-description";
    public static final String EXTRA_KEY_ORDER_CONTACT_NUMBER = "extra-key-order-contact-number";
    public static final String EXTRA_KEY_ORDER_DELIVERY_LOCATION = "extra-key-order-delivery-location";
    public static final String EXTRA_KEY_ORDER_DELIVERY_ADDRESS = "extra-key-order-delivery-address";
    public static final String EXTRA_KEY_ORDER_DELIVERY_DATE_AND_TIME = "extra-key-order-delivery-date-and-time";
    public static final String EXTRA_KEY_ORDER_DELIVERY_DATE = "extra-key-order-delivery-date";
    public static final String EXTRA_KEY_ORDER_DELIVERY_TIME = "extra-key-order-delivery-time";
    public static final String EXTRA_KEY_ORDER_SUBTOTAL = "extra-key-order-subtotal";
    public static final String EXTRA_KEY_ORDER_DELIVERY_CHARGES = "extra-key-order-delivery-charges";
    public static final String EXTRA_KEY_ORDER_TOTAL_AMOUNT = "extra-key-order-amount";
    public static final String EXTRA_KEY_ORDER_BAKER_EARNING = "extra-key-order-baker-earning";
    public static final String EXTRA_KEY_ORDER_COMPANY_EARNING = "extra-key-order-company-earning";
    public static final String EXTRA_KEY_ORDER_PAYMENT_METHOD = "extra-key-order-payment-method";
    public static final String EXTRA_KEY_ORDER_REJECTED_REASON = "extra-key-order-rejected-reason";
    public static final String EXTRA_KEY_ORDER_ORDER_RATING = "extra-key-order-order-rating";
    public static final String EXTRA_KEY_ORDER_USER_ID = "extra-key-order-user-id";
    public static final String EXTRA_KEY_ORDER_USER_NAME = "extra-key-order-user-name";
    public static final String EXTRA_KEY_ORDER_USER_EMAIL = "extra-key-order-user-email";
    public static final String EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER = "extra-key-order-data-load-from-server";
    public static final String EXTRA_KEY_ORDER_ACTIVITY_MODE = "extra-key-order-activity-mode";

    // Other constants
    public static final String MOBILE_NUMBER_PAK_PREFIX = "+92";
    public static final String KEY_IS_HELP_OPENED_FOR_REPORTING = "key-is-help-opened-for-reporting";
    public static final int ACTIVITY_MODE_ACCEPT = 01;
    public static final int ACTIVITY_MODE_REJECT = 02;
    public static final int ACTIVITY_MODE_RE_ACCEPT = 03;
}

