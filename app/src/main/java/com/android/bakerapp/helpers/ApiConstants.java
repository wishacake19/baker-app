package com.android.bakerapp.helpers;

public class ApiConstants {

    // Base URL
    private static final String BASE_URL = "http://feedpakistan.com/wishacake/v1/";
//    private static final String BASE_URL = "http://192.168.0.105/wishacake/v1/";

    // JSON response keys
    public static final String KEY_STATUS = "status";
    public static final String KEY_ERROR_CODE = "errorCode";
    public static final String KEY_RESPONSE = "response";

    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_ERROR = "error";

    // POST Params
    public static final String PARAM_ID = "id";
    public static final String PARAM_BAKER_ID = "baker_id";
    public static final String PARAM_FIREBASE_TOKEN = "firebase_token";
    public static final String PARAM_IMAGE = "image";
    public static final String PARAM_FIRST_NAME = "first_name";
    public static final String PARAM_LAST_NAME = "last_name";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_MOBILE_NUMBER = "mobile_number";
    public static final String PARAM_CNIC = "cnic";
    public static final String PARAM_PIN = "pin";
    public static final String PARAM_CNIC_FRONT = "cnic_front";
    public static final String PARAM_CNIC_BACK = "cnic_back";
    public static final String PARAM_LOCATION_ID = "location_id";
    public static final String PARAM_LOCATION_NAME = "location_name";
    public static final String PARAM_LOCATION_ADDRESS = "location_address";
    public static final String PARAM_LOCATION_LATITUDE = "location_latitude";
    public static final String PARAM_LOCATION_LONGITUDE = "location_longitude";
    public static final String PARAM_SLIDER_IMAGE1 = "slider_image1";
    public static final String PARAM_SLIDER_IMAGE2 = "slider_image2";
    public static final String PARAM_SLIDER_IMAGE3 = "slider_image3";
    public static final String PARAM_RATING = "rating";
    public static final String PARAM_ACCOUNT_STATUS = "account_status";
    public static final String PARAM_ACTIVE_STATUS = "active_status";
    public static final String PARAM_REVIEW = "review";
    public static final String PARAM_CREATED_AT = "created_at";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_ORDER_ID = "order_id";
    public static final String PARAM_ORDER_FOR = "order_for";
    public static final String PARAM_IMAGE1 = "image1";
    public static final String PARAM_IMAGE2 = "image2";
    public static final String PARAM_IMAGE3 = "image3";
    public static final String PARAM_IMAGE4 = "image4";
    public static final String PARAM_IMAGE5 = "image5";
    public static final String PARAM_IMAGE6 = "image6";
    public static final String PARAM_IMAGE7 = "image7";
    public static final String PARAM_IMAGE8 = "image8";
    public static final String PARAM_IMAGE9 = "image9";
    public static final String PARAM_IMAGE10 = "image10";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_QUANTITY = "quantity";
    public static final String PARAM_POUNDS = "pounds";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_CONTACT_NUMBER = "contact_number";
    public static final String PARAM_DELIVERY_LOCATION = "delivery_location";
    public static final String PARAM_DELIVERY_ADDRESS = "delivery_address";
    public static final String PARAM_DELIVERY_DATE = "delivery_date";
    public static final String PARAM_DELIVERY_TIME = "delivery_time";
    public static final String PARAM_SUBTOTAL = "subtotal";
    public static final String PARAM_DELIVERY_CHARGES = "delivery_charges";
    public static final String PARAM_TOTAL_AMOUNT = "total_amount";
    public static final String PARAM_PAYMENT_METHOD = "payment_method";
    public static final String PARAM_BAKER_EARNING = "baker_earning";
    public static final String PARAM_COMPANY_EARNING = "company_earning";
    public static final String PARAM_SLIDER_IMAGE_ATTR = "slider_image_attr";
    public static final String PARAM_SLIDER_IMAGE = "slider_image";
    public static final String PARAM_REJECTED_REASON = "rejected_reason";
    public static final String PARAM_ORDER_RATING = "order_rating";

    // API URLs
    public static final String API_SIGN_UP = BASE_URL + "create-baker.php";
    public static final String API_LOGIN = BASE_URL + "login-baker.php";
    public static final String API_VALIDATE = BASE_URL + "validate-baker.php";
    public static final String API_UPDATE = BASE_URL + "update-baker.php";
    public static final String API_FORGOT_PIN = BASE_URL + "forgot-pin-baker.php";
    public static final String API_UPDATE_ACTIVE_STATUS = BASE_URL + "update-active-status.php";
    public static final String API_GET_REVIEWS = BASE_URL + "get-reviews.php";
    public static final String API_CHANGE_PIN = BASE_URL + "change-pin-baker.php";
    public static final String API_GET_ORDERS = BASE_URL + "get-orders.php";
    public static final String API_LOGOUT_BAKER = BASE_URL + "logout-baker.php";
    public static final String API_GET_SINGLE_ORDER = BASE_URL + "get-single-order.php";
    public static final String API_GET_BAKER_EARNING = BASE_URL + "get-baker-earning.php";
    public static final String API_GET_BAKER_ORDERS_COUNT = BASE_URL + "get-baker-orders-count.php";
    public static final String API_UPDATE_BAKER_SLIDER_IMAGE = BASE_URL + "update-baker-slider-image.php";
    public static final String API_UPDATE_ORDER_STATUS = BASE_URL + "update-order-status.php";

    // Post param values
    public static final String PARAM_VALUE_OFFLINE = "Offline";
    public static final String PARAM_VALUE_ONLINE = "Online";
    public static final String PARAM_VALUE_REMOVE = "Remove";
    public static final String PARAM_VALUE_NEW = "New";
    public static final String PARAM_VALUE_ACCEPTED = "Accepted";
    public static final String PARAM_VALUE_RE_ACCEPT = "Re-accept";
    public static final String PARAM_VALUE_REJECTED = "Rejected";
    public static final String PARAM_VALUE_CONFIRMED = "Confirmed";
    public static final String PARAM_VALUE_CANCELLED = "Cancelled";
    public static final String PARAM_VALUE_ONGOING = "Ongoing";
    public static final String PARAM_VALUE_COMPLETED = "Completed";
    public static final String PARAM_VALUE_CASH = "Cash";
    public static final String PARAM_VALUE_CREDIT_DEBIT_CARD = "Card";
    public static final String PARAM_VALUE_BAKER = "Baker";
}
