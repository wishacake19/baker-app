package com.android.bakerapp.helpers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyRequestHandlerSingleton {

    private static VolleyRequestHandlerSingleton sInstance;
    private static Context sContext;

    private RequestQueue mRequestQueue;

    private VolleyRequestHandlerSingleton(Context context) {
        sContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyRequestHandlerSingleton getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new VolleyRequestHandlerSingleton(context);
        }
        return sInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(sContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }
}
