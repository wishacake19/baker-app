package com.android.bakerapp.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SharedPrefSingleton {

    private static SharedPrefSingleton sInstance;
    private static Context sContext;

    private SharedPrefSingleton(Context context) {
        sContext = context;
    }

    public static synchronized SharedPrefSingleton getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPrefSingleton(context);
        }
        return sInstance;
    }

    public void saveBakerData(String id, String image, String firstName, String lastName, String email, String mobileNumber, String cnic,
                              String pin, String locationId, String locationName, String locationAddress,
                              String locationLatitude, String locationLongitude, String sliderImage1, String sliderImage2,
                              String sliderImage3, String rating, String earning, String activeStatus) {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_BAKER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_ID, id);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_IMAGE, image);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_FIRST_NAME, firstName);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_LAST_NAME, lastName);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_EMAIL, email);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_MOBILE_NUMBER, mobileNumber);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_CNIC, cnic);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_PIN, pin);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ID, locationId);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_NAME, locationName);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ADDRESS, locationAddress);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LATITUDE, locationLatitude);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LONGITUDE, locationLongitude);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE1, sliderImage1);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE2, sliderImage2);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE3, sliderImage3);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_RATING, rating);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_EARNING, earning);
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_ACTIVE_STATUS, activeStatus);
        editor.apply();
    }

    public void saveBakerActiveStatus(String activeStatus) {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_BAKER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_ACTIVE_STATUS, activeStatus);
        editor.apply();
    }

    public void saveBakerEarning(String earning) {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_BAKER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SHARED_PREF_KEY_BAKER_EARNING, earning);
        editor.apply();
    }

    public boolean isBakerLoggedIn() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_BAKER, Context.MODE_PRIVATE);
        if (preferences.getString(Constants.SHARED_PREF_KEY_BAKER_ID, null) != null) {
            return true;
        }
        return false;
    }

    public boolean isBakerAccountCreationInProcess() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_BAKER, Context.MODE_PRIVATE);
        if (preferences.getString(Constants.SHARED_PREF_KEY_BAKER_MOBILE_NUMBER, null) != null) {
            return true;
        }
        return false;
    }

    public HashMap<String, String> getBakerData() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_BAKER, Context.MODE_PRIVATE);
        HashMap<String, String> bakerData = new HashMap<>();
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_ID, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_ID, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_IMAGE, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_IMAGE, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_FIRST_NAME, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_FIRST_NAME, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_LAST_NAME, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_LAST_NAME, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_EMAIL, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_EMAIL, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_MOBILE_NUMBER, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_MOBILE_NUMBER, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_CNIC, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_CNIC, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_PIN, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_PIN, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ID, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ID, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_LOCATION_NAME, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_NAME, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ADDRESS, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_ADDRESS, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LATITUDE, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LATITUDE, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LONGITUDE, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_LOCATION_LONGITUDE, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE1, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE1, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE2, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE2, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE3, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_SLIDER_IMAGE3, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_RATING, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_RATING, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_EARNING, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_EARNING, null));
        bakerData.put(Constants.SHARED_PREF_KEY_BAKER_ACTIVE_STATUS, preferences.getString(Constants.SHARED_PREF_KEY_BAKER_ACTIVE_STATUS, null));
        return bakerData;
    }

    public void clearBakerData() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_BAKER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public int getUniqueNotificationRequestCode() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_BAKER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        int requestCode = preferences.getInt(Constants.SHARED_PREF_KEY_NOTIFICATION_UNIQUE_REQUEST_CODE, 0) + 1;
        editor.putInt(Constants.SHARED_PREF_KEY_NOTIFICATION_UNIQUE_REQUEST_CODE, requestCode);
        editor.apply();
        return requestCode;
    }
}

