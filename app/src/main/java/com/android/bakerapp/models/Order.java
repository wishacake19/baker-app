package com.android.bakerapp.models;

import java.util.List;

public class Order {

    private String mId;
    private List<String> mImageUrls;
    private long mDate;
    private String mStatus, mQuantity, mPounds, mDescription, mContactNumber, mDeliveryLocation, mDeliveryAddress, mDeliveryDate, mDeliveryTime;
    private double mSubTotal, mDeliveryCharges, mTotalAmount, mBakerEarning, mCompanyEarning, mOrderRating;
    private String mPaymentMethod, mRejectedReason, mUserId, mUserName, mUserEmail;

    public Order(String id, List<String> imageUrls, long date, String status, String quantity, String pounds, String description,
                 String contactNumber, String deliveryLocation, String deliveryAddress, String deliveryDate, String deliveryTime,
                 double subTotal, double deliveryCharges, double totalAmount, double bakerEarning, double companyEarning,
                 String paymentMethod, String rejectedReason, double orderRating, String userId, String userName, String userEmail) {
        mId = id;
        mImageUrls = imageUrls;
        mDate = date;
        mStatus = status;
        mQuantity = quantity;
        mPounds = pounds;
        mDescription = description;
        mContactNumber = contactNumber;
        mDeliveryLocation = deliveryLocation;
        mDeliveryAddress = deliveryAddress;
        mDeliveryDate = deliveryDate;
        mDeliveryTime = deliveryTime;
        mSubTotal = subTotal;
        mDeliveryCharges = deliveryCharges;
        mTotalAmount = totalAmount;
        mBakerEarning = bakerEarning;
        mCompanyEarning = companyEarning;
        mPaymentMethod = paymentMethod;
        mRejectedReason = rejectedReason;
        mOrderRating = orderRating;
        mUserId = userId;
        mUserName = userName;
        mUserEmail = userEmail;
    }

    public String getId() {
        return mId;
    }

    public List<String> getImageUrls() {
        return mImageUrls;
    }

    public long getDate() {
        return mDate;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public String getPounds() {
        return mPounds;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getContactNumber() {
        return mContactNumber;
    }

    public String getDeliveryLocation() {
        return mDeliveryLocation;
    }

    public String getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public String getDeliveryDate() {
        return mDeliveryDate;
    }

    public String getDeliveryTime() {
        return mDeliveryTime;
    }

    public double getSubTotal() {
        return mSubTotal;
    }

    public double getDeliveryCharges() {
        return mDeliveryCharges;
    }

    public double getTotalAmount() {
        return mTotalAmount;
    }

    public double getBakerEarning() {
        return mBakerEarning;
    }

    public double getCompanyEarning() {
        return mCompanyEarning;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public String getRejectedReason() {
        return mRejectedReason;
    }

    public double getOrderRating() {
        return mOrderRating;
    }

    public String getUserId() {
        return mUserId;
    }

    public String getUserName() {
        return mUserName;
    }

    public String getUserEmail() {
        return mUserEmail;
    }
}
