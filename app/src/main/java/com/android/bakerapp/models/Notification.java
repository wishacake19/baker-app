package com.android.bakerapp.models;

public class Notification {

    private String mTitle, mMessage, mImageUrl, mAction, mActionDestination, mOrderStatus, mOrderId;
    private long mWhen;

    public Notification(String title, String message) {
        mTitle = title;
        mMessage = message;
    }

    public Notification(String title, String message, String imageUrl, String action, String actionDestination,
                        String orderStatus, String orderId, long when) {
        mTitle = title;
        mMessage = message;
        mImageUrl = imageUrl;
        mAction = action;
        mActionDestination = actionDestination;
        mOrderStatus = orderStatus;
        mOrderId = orderId;
        mWhen = when;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getAction() {
        return mAction;
    }

    public String getActionDestination() {
        return mActionDestination;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public long getWhen() {
        return mWhen;
    }
}
